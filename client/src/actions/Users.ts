import axios from "axios";
import setAuthorizationToken from "../utils/setAuthorizationToken";
import * as jwt from "jsonwebtoken";
import { Actions } from "./Actions";
import { Status } from "./Status";

export function updateProfile(userData) {
    return dispatch => {
        dispatch({
            type: Actions.SET_CURRENT_USER,
            user: {},
            status: Status.LOADING
        });
        return axios
            .post("/api/users/profile/update", userData)
            .then(res => {
                const token = res.data.token;
                localStorage.setItem("jwtToken", token);
                setAuthorizationToken(token);
                dispatch({
                    type: Actions.SET_CURRENT_USER,
                    user: jwt.decode(token),
                    status: Status.FETCHED
                });
            })
            .catch(err => {
                dispatch({
                    type: Actions.SET_CURRENT_USER,
                    user: {},
                    status: Status.ERROR
                });
            });
    };
}

export function fetchUsers(userData) {
    return dispatch => {
        dispatch({
            type: Actions.SET_USERS,
            users: [],
            status: Status.LOADING
        });
        return axios
            .post("/api/users/fetchAll", userData)
            .then(res => {
                dispatch({
                    type: Actions.SET_USERS,
                    users: res.data.users,
                    status: Status.FETCHED
                });
            })
            .catch(err => {
                dispatch({
                    type: Actions.SET_USERS,
                    users: [],
                    status: Status.ERROR
                });
            });
    };
}

export function findUserByID(userId) {
    return dispatch => {
        dispatch({
            type: Actions.SET_USERS,
            users: {},
            status: Status.LOADING
        });
        return axios
            .get("api/users/findUserByID/" + userId)
            .then(res => {
                dispatch({
                    type: Actions.SET_USERS,
                    users: res.data.user,
                    status: Status.FETCHED
                });
            })
            .catch(err => {
                dispatch({
                    type: Actions.SET_USERS,
                    users: {},
                    status: Status.ERROR
                });
            });
    };
}

export function updateUser(userData) {
    return dispatch => {
        dispatch({
            type: Actions.SET_USERS,
            users: {},
            status: Status.LOADING
        });
        return axios
            .post("/api/users/user/update", userData)
            .then(res => {
                dispatch({
                    type: Actions.SET_USERS,
                    users: res.data.users,
                    status: Status.FETCHED
                });
            })
            .catch(err => {
                dispatch({
                    type: Actions.SET_USERS,
                    users: {},
                    status: Status.ERROR
                });
            });
    };
}

export function addUser(userData) {
    return dispatch => {
        dispatch({
            type: Actions.SET_USERS,
            users: {},
            status: Status.LOADING
        });
        return axios
            .post("/api/users/add", userData)
            .then(res => {
                dispatch({
                    type: Actions.SET_USERS,
                    users: {},
                    status: Status.FETCHED
                });
            })
            .catch(err => {
                dispatch({
                    type: Actions.SET_USERS,
                    users: {},
                    status: Status.ERROR
                });
            });
    };
}

export function deleteUser(userId) {
    return dispatch => {
        return axios
            .post("/api/users/user/delete/" + userId)
            .catch(err => {
                dispatch({
                    type: Actions.SET_USERS,
                    user: [],
                    status: Status.ERROR
                });
            });
    };
}
