const path = require("path")
const HtmlWebpackPlugin = require("html-webpack-plugin")

module.exports = {
    entry: path.join(__dirname,'src','index.tsx'),
    output: {
        path: path.join(__dirname, "build"),
        filename: "bundle.js"
    },

    // Enable sourcemaps for debugging webpack's output.
    devtool: "source-map",

    resolve: {
        // Add '.ts' and '.tsx' as resolvable extensions.
        extensions: [".ts", ".tsx", ".js", ".jsx", ".json"]
    },
    node : {
        net : "empty",
        dns : "empty"
    },
    module: {
        loaders: [
            // All files with a '.ts' or '.tsx' extension will be handled by 'awesome-typescript-loader'.
            { test: /\.tsx?$/, loader: "awesome-typescript-loader", exclude: /node_modules/, include: __dirname},
            { test: /\.(json)$/, loaders: [ 'json-loader' ], include: __dirname},
            { test: /\.js$/, loaders:[ "babel-loader"], exclude : /node_modules/, include: __dirname},
            { test: /\.(less|less)$/,  loader: `style-loader!css-loader!less-loader?${JSON.stringify({ modifyVars : require("./package.json").theme})}` }
          
        ]
    },

    // When importing a module whose path matches one of the following, just
    // assume a corresponding global variable exists and use that instead.
    // This is important because it allows us to avoid bundling all of our
    // dependencies, which allows browsers to cache those libraries between builds.
    // externals: {
    //     "react": "React",
    //     "react-dom": "ReactDOM"
    // }, 
    // don't allow to render the even simple components


    plugins: [
        new HtmlWebpackPlugin({
          template: path.join(__dirname, 'index.html'), // Load a custom template
          inject: 'body' // Inject all scripts into the body
        })
    ]
};