import * as React from "react";
import {
    HashRouter as Router,
    Redirect,
    Switch,
    Route
} from "react-router-dom";

import { LeftMenuLayout } from "./layout";
import { Routes as LoginRouter } from "./login/Routes";
import { Routes as LogoutRouter } from "./logout/Routes";
import { Routes as SettingsRouter } from "./settings/Routes";
import { Routes as AdminRoutes } from "./admin/Routes"
import { Routes as HomeRoutes} from "./home/Routes"
import { Routes as MonitoringRoutes} from "./monitoring/Routes"

import requireAuth from "../utils/requireAuth";
import requirePerm from "../utils/requirePerm"

export class Routes extends React.Component<any, any> {
    render() {
        return (
            <Router>
                <Switch>
                    <Route path="/login" component={LoginRouter} />
                    <Route path="/logout" component={LogoutRouter} />
                    <LeftMenuLayout>
                        <Switch>
                            <Redirect exact from="/" to="/home" />
                            <Route path="/home" component={requireAuth()(HomeRoutes)}/>
                            <Route path="/monitoring" component={requireAuth()(MonitoringRoutes)} />
                            <Route path="/settings" component={requireAuth()(SettingsRouter)}/>
                            <Route path="/admin" component={requirePerm()(requireAuth()(AdminRoutes))}/>
                        </Switch>
                    </LeftMenuLayout>
                </Switch>
            </Router>
        );
    }
}
