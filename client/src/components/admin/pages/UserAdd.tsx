import * as React from "react"
import {UserForm} from "../../shared-components/UserForm"
import {addUser} from "../../../actions/Users"
import {connect} from "react-redux"
import {Card} from "antd"

export interface UserAddOwnProps{
    auth : any,
    addUser : any
}

export const UserAdd = connect(
    state => {
        return {
            auth : state.auth,
        }
    },
    { addUser }
    
)(
    class UserAdd extends React.Component<any, any> {
        
        render(){
            
            return (
                <Card title="Ajouter un nouvel utilisateur" style={{ maxWidth : 800, margin : "auto" }}>
                <UserForm 
                auth={this.props.auth.user}
                actionProfile={this.props.addUser}
                user={null}
                />
                </Card>
            )
            
        }
        
    }
)