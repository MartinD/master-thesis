require("dotenv").config();
const couchbase = require("couchbase");
const curl = require("curlrequest");
const cluster = new couchbase.Cluster("http://cerhisprod.org:8091/");
cluster.authenticate(
    process.env.COUCHBASE_USERNAME,
    process.env.COUCHBASE_PASSWORD
);

let bucket = cluster.openBucket(process.env.COUCHBASE_BUCKET);

var N1qlQuery = couchbase.N1qlQuery;

export var Query = (query: any, callback) => {
    bucket.httpsQuery(N1qlQuery.fromString(query), callback);
};

export var httpsQuery = (query , callback) => {
    curl.request({
        url : "https://cerhisprod.org:18093/query/service",
        data : `statement=${query}&creds=[{"user":"${process.env.COUCHBASE_USERNAME}", "pass":"${process.env.COUCHBASE_PASSWORD}"}]"`,
        insecure: true,
    }, (err, data) => {
        if(!err) callback(JSON.parse(data).errors, JSON.parse(data).results, { status : JSON.parse(data).status, metrics : JSON.parse(data).metrics});
        else console.log("Requête HTTP non valide")
    })

}

const defaultCallbackCreate = (err, res) => {
    if (err) console.log("Erreur lors de la création");
    else console.log("Création terminée");
};

export const Create = (name, data, callback = defaultCallbackCreate) => {
    bucket.upsert(name, data, callback);
};

export const CreateMultiple = (data, callback = defaultCallbackCreate) => {
    var values = ``;
    console.log("DATA", data);
    for (var i in data) {
        values += `values ("${data[i].id}", ${JSON.stringify(data[i])}), `;
        if (data[i].type == "data") {
            if (data[i].smsType == "alert" || data[i].smsType == "noAlert") {
                console.log("SMS DATA", data[i]);
                values += `values ("${data[i].deviceKey}${
                    data[i].monitoringKey
                }", {"type":"alert", "id":"${data[i].deviceKey}${
                    data[i].monitoringKey
                }", "status":"${data[i].smsType}", "monitoringKey":"${
                    data[i].monitoringKey
                }", "deviceKey":"${data[i].deviceKey}"}), `;
            }
        }
    }
    values = values.slice(0, -2);
    const query = `upsert into CERHIS_MONITOR c1 (key, value) ${values} returning c1.*`;
    httpsQuery(query, callback);
};

const defaultCallbackUpdate = (err, res) => {
    if (err) console.log("Erreur lors de la mise à jour");
    else console.log("Mise à jour terminée");
};

export const Update = (name, data, callback = defaultCallbackUpdate) => {
    Create(name, data, callback); //upsert rajoute ou met à jour en fonction de si l'élément existe ou non
};

export const UpdateMultiple = (objects, callback = defaultCallbackUpdate) => {
    CreateMultiple(objects, callback);
};

const defaultCallbackDelete = (err, res) => {
    if (err) console.log("Erreur lors de la suppression");
    else console.log("Suppresion terminée");
};

export const Delete = (id, callback = defaultCallbackDelete) => {
    bucket.remove(id, callback);
};

export const DeleteMultiple = (objects, callback = defaultCallbackDelete) => {
    //Delete(objects[0].id, callback);
    const query = `delete from CERHIS_MONITOR c1 where c1.id="${
        objects[0].id
    }" or c1.deviceKey='${objects[0].id}'`;
    httpsQuery(query, callback);
};
