import { combineReducers } from "redux";

import auth from "./reducers/auth";
import users from "./reducers/users";
import monitoring from "./reducers/monitoredCenters";
import monitoringDevices from "./reducers/monitoringDevices";
import monitoringDeviceData from "./reducers/monitoringDeviceData";
import monitoringDeviceConfigs from "./reducers/monitoringDeviceConfigs";
import monitoringDeviceThresholds from "./reducers/monitoringDeviceThresholds";
import monitoringDeviceCheckTimes from "./reducers/monitoringDeviceCheckTimes";
import monitoringDeviceUsers from "./reducers/monitoringDeviceUsers";
import monitoringUsers from "./reducers/monitoringUsers";
import monitoringAlerts from "./reducers/monitoringAlerts";

export default combineReducers({
    auth,
    users,
    monitoring,
    monitoringDevices,
    monitoringDeviceData,
    monitoringDeviceConfigs,
    monitoringDeviceThresholds,
    monitoringDeviceCheckTimes,
    monitoringDeviceUsers,
    monitoringUsers,
    monitoringAlerts
});
