require("dotenv").config();
var express = require("express");
import authenticate from "../middlewares/authenticate";
import { httpsQuery, Create, Delete } from "../models/db";
import * as uuid from "uuid"
import permissions from "../middlewares/permissions";
import * as _ from "lodash"

let router = express.Router();

router.get("/fetchAll", authenticate, (req, res) => {
    const query = "select c1.* from CERHIS_MONITOR c1 where c1.type='monitoring'";

    httpsQuery(query, (err, monitoring, meta) => {
        if (!err) {
            res.json({ monitoring });
        }
    });
});

router.post("/fetch", authenticate, (req, res) => {
    const query = `select c1.* from CERHIS_MONITOR c1 where c1.type='monitoring' and c1.id in 
    (select raw monitoringKey from CERHIS_MONITOR where type = 'usersMonitoring' and userKey='${
        req.body.userKey
    }')`;

    httpsQuery(query, (err, monitoring, meta) => {
        if (!err) {
            res.json({ monitoring });
        } else {
            throw err;
        }
    });
});

router.get("/findById/:id", authenticate, (req, res) => {
    const query = `select c1.* from CERHIS_MONITOR c1 where c1.id='${
        req.params.id
    }' and c1.type='monitoring'`;

    httpsQuery(query, (err, monitoring, meta) => {
        if (!err) {
            res.json({ monitoring });
        }
    });
});

/*router.post("/devices/fetch", authenticate, (req, res) => {
    const query = `select c1.* from CERHIS_MONITOR c1 where c1.idMonitoring = '${
        req.body.idMonitoring
    }' and c1.type = "device" and c1.id in (select raw deviceKey from CERHIS_MONITOR where type = 'deviceUser' and userKey='${
        req.body.userKey
    }')`;

    httpsQuery(query, (err, monitoringDevices, meta) => {
        if (!err) {
            res.json({ monitoringDevices });
        } else {
            throw err;
        }
    });
});*/

router.post("/devices/fetchAll", authenticate, (req, res) => {
    const query = `select c1.* from CERHIS_MONITOR c1 where c1.monitoringKey = '${
        req.body.monitoringKey
    }' and c1.type = "device"`;

    httpsQuery(query, (err, monitoringDevices, meta) => {
        if (!err) {
            res.json({ monitoringDevices });
        } else {
            throw err;
        }
    });
});

router.post("/device/data/fetch", authenticate, (req, res) => {
    var data = [];
    const query = `select c1.* from CERHIS_MONITOR c1 where c1.type='data' and c1.deviceKey in 
    (select raw c2.id from CERHIS_MONITOR c2 where c2.type='device' and c2.monitoringKey='${req.body.monitoringKey}')`;

    httpsQuery(query, (err, monitoringDeviceData, meta) => {
        if(!err){
            res.json({monitoringDeviceData})
        } else {
            throw err
        }
    })

});

router.post("/device/thresholds/fetch", authenticate, (req, res) => {
    var thresholds = [];
    for (var i = 0; i < req.body.length; i++) {
        const query = `select c1.* from CERHIS_MONITOR c1 where c1.type='threshold' and c1.deviceKey='${
            req.body[i].id
        }'`;
        httpsQuery(query, (err, monitoringDeviceThresholds, meta) => {
            if (!err) {
                thresholds.push(monitoringDeviceThresholds);
            } else {
                throw err;
            }
            if (thresholds.length == req.body.length) {
                res.json({ monitoringDeviceThresholds: thresholds });
            }
        });
    }
});

router.post("/device/configs/fetch", authenticate, (req, res) => {
    var configs = [];
    for (var i = 0; i < req.body.length; i++) {
        const query = `select c1.* from CERHIS_MONITOR c1 where c1.type='config' and c1.deviceKey='${
            req.body[i].id
        }'`;
        httpsQuery(query, (err, monitoringDeviceConfigs, meta) => {
            if (!err) {
                configs.push(monitoringDeviceConfigs);
            } else {
                throw err;
            }
            if (configs.length == req.body.length) {
                res.json({ monitoringDeviceConfigs: configs });
            }
        });
    }
});

router.post("/device/checkTimes/fetch", authenticate, (req, res) => {
    var checkTimes = [];
    for (var i = 0; i < req.body.length; i++) {
        const query = `select c1.* from CERHIS_MONITOR c1 where c1.type='checkTime' and c1.deviceKey='${
            req.body[i].id
        }'`;
        httpsQuery(query, (err, monitoringDeviceCheckTimes, meta) => {
            if (!err) {
                checkTimes.push(monitoringDeviceCheckTimes);
            } else {
                throw err;
            }
            if (checkTimes.length == req.body.length) {
                res.json({ monitoringDeviceCheckTimes: checkTimes });
            }
        });
    }
});

router.post("/devicesUsers/fetch", authenticate, (req, res) => {

    // const query = `select c1.* from CERHIS_MONITOR c1 where c1.type='deviceUser' and c1.userKey='${req.body.userKey}' and c1.userKey in 
    // (select raw userKey from CERHIS_MONITOR where type='usersMonitoring' and monitoringKey='${req.body.monitoringKey}')`;
    const query = `select c1.* from CERHIS_MONITOR c1 where c1.type='deviceUser' and c1.userKey='${req.body.userKey}' and c1.deviceKey in 
    (select raw id from CERHIS_MONITOR where type="device" and monitoringKey='${req.body.monitoringKey}')`;

    httpsQuery(query, (err, monitoringDeviceUsers, meta) => {
        if (!err) {
            res.json({ monitoringDeviceUsers });
        } else {
            throw err;
        }
    });
});

router.post("/devices/updateUsers", authenticate, (req, res) => {
    const query = `delete from CERHIS_MONITOR c1 where c1.type='deviceUser' and c1.userKey="${req.body.userKey}" and c1.deviceKey in (select raw id from CERHIS_MONITOR where type="device" and monitoringKey="${req.body.monitoringKey}")`
    httpsQuery(query, (err, meta) =>{
        if(!err){
            if(req.body.monitoringDeviceUserKeys.length > 0){
                var values=``
                for(var key in req.body.monitoringDeviceUserKeys){
                    let id = uuid.v4();
                    values += `values ("${id}", {"id":"${id}", "type":"deviceUser", "deviceKey":"${req.body.monitoringDeviceUserKeys[key]}", "userKey":"${req.body.userKey}"}), `
                }
                values = values.slice(0, -2);
                const query2 = `insert into CERHIS_MONITOR c1 (key, value) ${values} returning c1.*`
                if(_.includes(req.body.monitoringUserKeys, req.body.userKey) || _.includes(req.body.monitoringUserKeys, req.body.monitoringKey)){
                    httpsQuery(query2, (err, monitoringDeviceUsers, meta) => {
                        if(!err){
                            res.json({monitoringDeviceUsers, status : "Données enregistrées"})
                        } else {
                            res.status(401).json({status : "Impossible de rajouter les abonnements"})
                        }
                    })
                } else {
                    res.status(401).json({monitoringDeviceUsers: [], status : "Vous ne pouvez pas ajouter des services à un utilisateur non abonné au centre!"})
                } 
            } else {
                res.json({monitoringDeviceUsers : []})
            }
        } else {
            res.status(401).json({status : "Impossible de supprimer les abonnements"})
        } 
})})

router.post("/updateUsers", authenticate, permissions, (req, res) => {
    let query;
    if(req.body.userKey != undefined){
        query = `delete from CERHIS_MONITOR c1 where c1.type='usersMonitoring' and c1.userKey="${req.body.userKey}"`
    } else{
        query = `delete from CERHIS_MONITOR c1 where c1.type='usersMonitoring' and c1.monitoringKey="${req.body.monitoringKey}"`
    }

    httpsQuery(query, (err, meta) => {
        if(!err){
            if(req.body.monitoringUserKeys.length > 0){
                var values = ``
                for(var key in req.body.monitoringUserKeys){
                    let id = uuid.v4();
                    if(req.body.userKey != undefined){          //permet de différencier l'abonnement à partir des utilisateurs de l'abonnement à partir des centres respectivement
                        values += `values ("${id}", {"id":"${id}", "type":"usersMonitoring", "userKey":"${req.body.userKey}", "monitoringKey":"${req.body.monitoringUserKeys[key]}"}), `
                    } else {
                        values += `values ("${id}", {"id":"${id}", "type":"usersMonitoring", "userKey":"${req.body.monitoringUserKeys[key]}", "monitoringKey":"${req.body.monitoringKey}"}), `
                    }
                }
                values = values.slice(0, -2)
                const query2 = `insert into CERHIS_MONITOR c1 (key, value) ${values} returning c1.*`
                httpsQuery(query2, (err, monitoringUsers, meta) => {
                    if(!err){
                        res.json({monitoringUsers, status : "Abonnements au centre enregistrés"})
                    } else {
                        res.json({status : "Impossible de rajouter les abonnements"})
                    }
                })
            } else {
                res.json({monitoringUsers : [], status : "Pas d'utilisateur à abonner"})
            }
        } else {
            res.json({status : "Impossible de supprimer les abonnements"})
        }
    })
})

router.post("/users/fetch", authenticate, permissions, (req, res) => {
    let query;
    if(req.body.monitoringKey != undefined){    //permet de différencier l'abonnement à partir des centres de l'abonnement à partir des utilisateurs respectivement
        query = `select c1.* from CERHIS_MONITOR c1 where c1.type='usersMonitoring' and c1.monitoringKey='${req.body.monitoringKey}'`
    } else {
        query = `select c1.* from CERHIS_MONITOR c1 where c1.type='usersMonitoring' and c1.userKey='${req.body.userKey}'`
    }

    httpsQuery(query, (err, monitoringUsers, meta) => {
        if(!err){
            res.json({monitoringUsers})
        } else {
            throw err;
        }
    })
})

router.post("/alerts/fetch", authenticate, (req, res) => {
    const query = `select c1.* from CERHIS_MONITOR c1 where c1.type="alert" and c1.status="alert" and c1.monitoringKey in ${JSON.stringify(req.body.monitoring)}`

    httpsQuery(query, (err, monitoringAlerts, meta) => {
        if(!err){
            res.json({monitoringAlerts})
        } else {
            throw err;
        }
    })
})

export default router;
