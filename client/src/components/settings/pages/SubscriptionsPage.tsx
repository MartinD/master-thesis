import * as React from "react"
import { connect } from "react-redux"
import { Table, Icon, Button, Spin, Modal, message, Card, Steps, Popover } from "antd";
import { withRouter, Redirect, Link } from "react-router-dom";
import { Status } from "../../../actions/Status"
import { fetchMonitoredCenters, fetchAllDevicesByCenter, findMonitoredCenterById, fetchDeviceUsersByUser, updateDeviceSubscriptions } from "../../../actions/Monitoring"
import { SubscriptionsForm } from "../../shared-components/SubscriptionsForm";

const Step = Steps.Step
const customDot = (dot, { status, index, title, description }) => (
    <Popover content={<span>{description}</span>}>
      {dot}
    </Popover>
  );

export const SubscriptionsPage = connect(
    state => {
        return{
            auth : state.auth,
            monitoring : state.monitoring,
            monitoringDevices: state.monitoringDevices,
            monitoringDeviceUsers : state.monitoringDeviceUsers
        }
    },
    { fetchMonitoredCenters, fetchAllDevicesByCenter, findMonitoredCenterById, fetchDeviceUsersByUser, updateDeviceSubscriptions }
)(
    class SubscriptionsPage extends React.Component<any, any>{

        state = {
            isLoading: false,
            selectedRowKeys: [],
            monitoringKey : null,
            userKey : this.props.auth.user.id,
            step : 0
        };

        componentWillMount(){
            this.props.fetchMonitoredCenters({
                userKey : this.props.auth.user.id
            });
        }

        updateSubscriptions = e => {
            e.preventDefault();
            var data = {
                monitoringDeviceUserKeys : this.state.selectedRowKeys,
                monitoringUserKeys : [this.state.userKey],
                monitoringKey : this.state.monitoringKey,
                userKey : this.state.userKey
            }
            this.setState({
                isLoading : true
            })
            this.props.updateDeviceSubscriptions(data).then(res => {
                this.setState({
                    isLoading: false,
                    errors: {}
                });
                message.success("Données enregistrées");
            })
            .catch(err => {
                this.setState({
                    isLoading: false,
                    errors: err.response.data.errors
                });
                message.error("Erreur lors de l'enregistrement");
            });
            // this.setState({
            //     isLoading: true,
            //     errors: {}
            // });
            // this.props.actionSubscribe(this.createOrUpdate(values)).then(res => {
            //     message.success("Données envoyées"); //TODO changer ceci pour éviter que l'utilisateur croient que les données sont enregistrées alors que le serveur n'a pas répondu
            //     this.setState({
            //         isLoading : false,
            //         errors : {}
            //     });
            // }).catch(err => {
            //     this.setState({
            //         isLoading : false,
            //         errors : err
            //     });
            //     message.error("Erreur lors de l'envoi des données");
            // })
        }

        render(){
            const columns = [
                {
                    title: "Nom",
                    dataIndex: "name",
                    key: "name",
                    // render: (text, record, index) => (
                    //     <h3>{text}</h3>
                    // )
                },
                {
                    title: "Responsable",
                    dataIndex: "responsible",
                    key: "responsible"
                },
                {
                    title: "Téléphone",
                    dataIndex: "phone",
                    key: "phone"
                }
            ];
            const data = [];
            const monitoring = this.props.monitoring.monitoring;
            for (var i = 0; i < monitoring.length; i++) {
                data.push({
                    key: monitoring[i].id,
                    name: monitoring[i].name,
                    responsible: monitoring[i].responsible,
                    phone: monitoring[i].phone
                });
            }
            const Title = () => {
                return (
                    <span className="monitoring-table-title">
                        Liste des centres de monitoring auxquels vous êtes abonnés ({this.props.monitoring.monitoring.length || 0})
                    </span>
                );
            };
            const columns2 = [
                {
                    title : "Nom",
                    dataIndex : "name",
                    key : "name"
                },
                {
                    title : "Adresse IP",
                    dataIndex : "ip",
                    key : "ip"
                },
                {
                    title : "Type de monitoring",
                    dataIndex : "type",
                    key : "type"
                }
            ]
            const data2 = [];
            const monitoringDevices = this.props.monitoringDevices.data;
            for(var i=0; i<monitoringDevices.length; i++){
                data2.push({
                    key : monitoringDevices[i].id,
                    name : monitoringDevices[i].name,
                    ip : monitoringDevices[i].ip,
                    type : monitoringDevices[i].deviceType
                })
            }
            const Title2 = () => {
                return (
                    <span className="devices-table-title">
                        Services auxquels vous êtes abonnés ({this.props.monitoringDeviceUsers.data.length || 0})
                    </span>
                );
            }
            const rowSelection = {
                onChange : (selectedRowKeys, selectedRows) => {
                    this.setState({selectedRowKeys})
                },
                selectedRowKeys : this.state.selectedRowKeys
            }

            const deviceTable = <Table
                title={Title2}
                columns={columns2}
                dataSource={data2}
                rowSelection={rowSelection}
                >
            </Table>
            return <Card title="Vos abonnements">
                <Spin spinning={this.props.monitoring.status === Status.LOADING || this.state.isLoading}>
                    <Steps current={this.state.step} progressDot={customDot}>
                        <Step
                            title="Centres"
                            description="Cliquez sur un centre de santé."
                        />
                        <Step
                            title="Services"
                            description="Sélectionnez les services."
                        />
                    </Steps>
                    <Table
                        title={Title}
                        onRow={(record : Object) => {
                            return {
                                onClick : () => {
                                    this.props.fetchAllDevicesByCenter({
                                        monitoringKey: record["key"]
                                    }).then(() => {
                                        this.setState({monitoringKey : record["key"],
                                    step : 1})
                                    });
                                    this.props.fetchDeviceUsersByUser({
                                        userKey: this.props.auth.user.id,
                                        monitoringKey: record["key"]
                                    }).then(() => {
                                        this.setState({
                                            selectedRowKeys : this.props.monitoringDeviceUsers.data.map(data => data.deviceKey),
                                            isLoading: false
                                        });
                                    });
                                }
                            }
                        }}
                        columns={columns}
                        dataSource={data}
                    />
                        {this.state.step == 1 ? 
                        deviceTable
                         : null}
                         <Button
                            disabled={this.state.step != 1}
                            onClick={this.updateSubscriptions}
                            type="primary"
                            htmlType="submit"
                            className="update-subscriptions"
                            style={{ width: "100%" }} >
                            Enregistrer les abonnements
                        </Button>
                </Spin>
            </Card>
        }

    }
)