import * as React from "react";
import * as _ from "lodash";
import * as moment from "moment"
import { connect } from "react-redux";
import {
    LineChart,
    Line,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
    Legend,
    ReferenceArea
} from "recharts";
import { Card, List, message, Spin, Icon } from "antd";
import {
    findMonitoredCenterById,
    fetchDevicesByCenter,
    fetchDeviceDataByDevice,
    fetchCheckTimeByDevice,
    fetchConfigByDevice,
    fetchThresholdByDevice,
    fetchDeviceUsersByUser,
    fetchAllDevicesByCenter,
    fetchMonitoringAlerts,
    testSMS
} from "../../../actions/Monitoring";
import { Status } from "../../../actions/Status";
import { Button } from "antd";

export const MedicalCenterData = connect(
    state => {
        return {
            auth: state.auth,
            monitoring: state.monitoring,
            monitoringDevices: state.monitoringDevices,
            monitoringDeviceData: state.monitoringDeviceData,
            monitoringDeviceConfigs : state.monitoringDeviceConfigs,
            monitoringDeviceCheckTimes : state.monitoringDeviceCheckTimes,
            monitoringDeviceThresholds : state.monitoringDeviceThresholds,
            monitoringDeviceUsers : state.monitoringDeviceUsers,
            monitoringAlerts : state.monitoringAlerts
        };
    },
    {
        findMonitoredCenterById,
        fetchDevicesByCenter,
        fetchDeviceDataByDevice,
        fetchCheckTimeByDevice,
        fetchConfigByDevice,
        fetchThresholdByDevice,
        fetchDeviceUsersByUser,
        fetchAllDevicesByCenter,
        fetchMonitoringAlerts,
        testSMS
    }
)(
    class MedicalCenterData extends React.Component<any, any> {

        constructor(props){
            super(props);
            this.state = {
                monitoring : {
                    name: "Centre",
                    address: "quelque part",
                    responsible: "",
                    phone: ""
                },
                monitoringDevices : [],
                monitoringDeviceCheckTimes : [],
                monitoringDeviceConfigs : [],
                monitoringDeviceThresholds : [],
                monitoringDeviceData : [],
                monitoringDeviceUsers : []
            }
        }

        componentWillMount() {
            const monitoringId = this.props.match.params["id"];
            this.props.fetchDeviceDataByDevice({
                userKey : this.props.auth.user.id,
                monitoringKey : monitoringId
            }).then(() => {
                this.setState({
                    monitoringDeviceData : this.props.monitoringDeviceData.data
                })
            })
            this.props.findMonitoredCenterById(monitoringId).then(()=> {
                this.setState({
                    monitoring : this.props.monitoring.monitoring[0]
                })
            }).then(() => {
                this.props.fetchAllDevicesByCenter({monitoringKey : monitoringId}).then(() => {
                    // this.props.fetchThresholdByDevice(this.props.monitoringDevices.monitoringDevices).then(() => {   //Dans un second temps
                    //     this.setState({
                    //         monitoringDeviceThresholds : this.props.monitoringDeviceThresholds.monitoringDeviceThresholds                    
                    //     })
                    // })
                    // this.props.fetchCheckTimeByDevice(this.props.monitoringDevices.monitoringDevices).then(() => {
                    //     this.setState({
                    //         monitoringDeviceCheckTimes : this.props.monitoringDeviceCheckTimes.monitoringDeviceCheckTimes
                    //     })
                    // });
                    // this.props.fetchConfigByDevice(this.props.monitoringDevices.monitoringDevices).then(() => {
                    //     this.setState({
                    //         monitoringDeviceConfigs : this.props.monitoringDeviceConfigs.monitoringDeviceConfigs  
                    //     })
                    // });
                    // this.props.fetchDeviceDataByDevice(this.props.monitoringDevices.monitoringDevices).then(() => {
                    //     this.setState({
                    //         monitoringDeviceData : this.props.monitoringDeviceData.monitoringDeviceData    
                    //     })
                    // });
                }).then(() => {
                    this.setState({
                        monitoringDevices : this.props.monitoringDevices.data
                    })
                })
                this.props.fetchDeviceUsersByUser({
                    userKey : this.props.auth.user.id,
                    monitoringKey : monitoringId
                }).then(() => {
                    this.setState({
                        monitoringDeviceUsers : this.props.monitoringDeviceUsers.data
                    })
                })
                this.props.fetchMonitoringAlerts({monitoring : [monitoringId]})
                
                
                
            });
        }

        isStatusFetched(){
            if(this.props.monitoringDevices.status == Status.FETCHED && 
                //this.props.monitoringDeviceConfigs.status == Status.FETCHED && 
                //this.props.monitoringDeviceCheckTimes.status == Status.FETCHED && 
                //this.props.monitoringDeviceThresholds.status == Status.FETCHED &&
                this.props.monitoringDeviceData.status == Status.FETCHED && this.props.monitoringDeviceUsers.status == Status.FETCHED)
                return true;
            return false;
        }

        // isStatusError(){
        //     if(this.props.monitoringDevices.status == Status.ERROR || this.props.monitoringDeviceConfigs.status == Status.ERROR || 
        //         this.props.monitoringDeviceCheckTimes.status == Status.ERROR || this.props.monitoringDeviceThresholds.status == Status.ERROR ||
        //         this.props.monitoringDeviceData.status == Status.ERROR)
        //         return true;
        //     return false;
        // }

        findGoodFuckingData(array, deviceKey){
            var data = []
            for(var i=0; i<array.length; i++){
                if(array[i].deviceKey == deviceKey){
                    data.push(array[i])
                }
            }
            data.sort((a, b) => {
                return new Date(a.timeStamp).getTime() - new Date(b.timeStamp).getTime();
            });  
            return data
        }

        testSMS = () => {
            const alertMsg = `create&&{"id":"testAlertMsg1", "timeStamp":"2018-04-11 11:21:30", "first":0, "second":0, "third":0, "deviceKey":"999", "type":"data", "smsType":"alert", "monitoringKey":"10"}`;
            const noAlerMsg = `create&&{"id":"testAlertMsg2", "timeStamp":"2018-04-11 11:23:30", "first":1, "second":1, "third":1, "deviceKey":"998", "type":"data", "smsType":"noAlert", "monitoringKey":"10"}`;;
            const reportMsg = `create&&{"id":"testAlertMsg3", "timeStamp":"2018-04-11 11:23:30", "first":1, "second":1, "third":1, "deviceKey":"999", "type":"data", "smsType":"report"}&&{"id":"testAlertMsg4", "timeStamp":"2018-04-11 11:23:30", "first":1, "second":1, "third":1, "deviceKey":"998", "type":"data", "smsType":"report"}`;
            this.props.testSMS(noAlerMsg);
        }

        render() {
            var deviceByType = {}
            this.state.monitoringDevices.forEach((elem, key) => { 
                key=elem.deviceType, deviceByType[key] ? deviceByType[key].push(elem) : (deviceByType[key] = [elem])
            })
            return (
                <div className="medical-center-data">
                    <div className="basic-info">
                        <Card title={this.state.monitoring.name}>
                            <Spin spinning={this.props.monitoring.status == Status.LOADING ? true : false}>
                                <div className="address">
                                    Adresse : {this.state.monitoring.address}
                                </div>
                                <div className="contact">
                                    <span className="responsible">Responsable : {this.state.monitoring.responsible}</span><br/>
                                    <span className="phone">Téléphone : {this.state.monitoring.phone}</span>
                                </div>
                                {/* <Button type="primary" onClick={this.testSMS}>Test SMS</Button> */}
                            </Spin>
                        </Card>
                        <MedicalCenterEvents
                            title={"Alertes"}
                            devices={_.filter(this.props.monitoringDevices.data, device => {
                                for(var i = 0; i<this.props.monitoringAlerts.data.length; i++){
                                    return device.id == this.props.monitoringAlerts.data[i].deviceKey
                                }
                            })}
                        />
                    </div>
                    <div className="charts">
                    {
                        this.isStatusFetched() ? Object.keys(deviceByType).map((device, index) => {
                            let title;
                            switch(device){
                                case "0":
                                    title = "Wifi"
                                    break;
                                case "1":
                                    title = "Présence des éléments sur le réseau"
                                    break;
                                case "2":
                                    title = "Etat du serveur"
                                    break;
                                case "3":
                                    title = "Charge de la batterie"
                                    break;
                                case "4":
                                    title = "Charge du smartphone"
                                    break;
                                case "5":
                                    title = "Réseau électrique"
                                    break;
                                case "6":
                                    title = "Panneaux solaires"
                                    break;
                            }
                            return <Card title={title} key={index} className="charts-by-type">
                            {deviceByType[device].map((elem, key) => {
                                const alert = _.includes(this.props.monitoringAlerts.data.map(monitoringAlert => monitoringAlert.deviceKey), elem["id"]);
                                return  <div key={key}>
                                <h3>{alert ? <span><Icon type="warning" /> </span>: null}{elem["name"]}</h3>
                                
                                <LineChart
                                    width={1500}
                                    height={300}
                                    data={this.findGoodFuckingData(this.state.monitoringDeviceData, elem.id)}
                                    margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
                                >
                                    <XAxis dataKey="timeStamp" />
                                    <YAxis />
                                    <CartesianGrid strokeDasharray="3 3" />
                                    <Tooltip />
                                    <Legend />
                                    <Line
                                        type="monotone"
                                        dataKey="first"
                                        stroke="#8884d8"
                                        strokeWidth={4}
                                    />
                                    {device == "2" ? <Line
                                        type="monotone"
                                        dataKey="second"
                                        stroke="#fda600"
                                        strokeWidth={4}
                                    /> : null}
                                    {device == "2" ? <Line
                                        type="monotone"
                                        dataKey="third"
                                        stroke="#9f2828"
                                        strokeWidth={4}
                                    /> : null}
                                </LineChart>
                            </div>
                                // <MedicalCenterGraph
                                //     device={elem}
                                //     deviceData={this.findGoodFuckingData(this.state.monitoringDeviceData, elem.id)}
                                //     deviceCheckTime={this.findGoodFuckingData(this.state.monitoringDeviceCheckTimes, elem.id)}
                                //     deviceThreshold={this.findGoodFuckingData(this.state.monitoringDeviceThresholds, elem.id)}
                                //     deviceConfig={this.findGoodFuckingData(this.state.monitoringDeviceConfigs, elem.id)}
                                //     key={key}
                                // />
                                
                            })}
                            </Card>
                        }) : <h1>Données en cours de chargement</h1>
                    }
                    </div>
                </div>
            );
        }
    }
);

interface MedicalCenterInfoOwnProps {
    title: string;
    address: any;
    responsible: string;
    responsiblePhone : string;
}

export class MedicalCenterInfo extends React.Component<
    MedicalCenterInfoOwnProps,
    any
> {
    render() {
        return (
            <Card title={this.props.title} >
                <div className="address">
                    Adresse : {this.props.address}
                </div>
                <div className="contact">
                    <span className="responsible">Responsable : {this.props.responsible}</span><br/>
                    <span className="phone">Téléphone : {this.props.responsiblePhone}</span>
                </div>
            </Card>
        );
    }
}

interface MedicalCenterEventsOwnProps {
    title: string;
    devices: string[]
}

export class MedicalCenterEvents extends React.Component<
    MedicalCenterEventsOwnProps,
    any
> {
    render() {
        console.log(this.props.devices)
        return (
            <Card title={this.props.title} style={{ width : 300 }}>
                <List
                    dataSource={this.props.devices}
                    itemLayout="horizontal"
                    renderItem={item => <List.Item style={{ color : "red" }}>{item.name} ({item.ip}) </List.Item>}
                />
            </Card>
        );
    }
}

export class MedicalCenterGraphs extends React.Component<any, any>{


    
}

interface MedicalCenterGraphOwnProps {
    device: any
    deviceData : any
    deviceConfig : any
    deviceThreshold : any
    deviceCheckTime : any
}

  
  
  

export class MedicalCenterGraph extends React.Component<
    MedicalCenterGraphOwnProps,
    any
> {

    constructor(props){
        super(props);
        this.state = {
            data : props.deviceData
        }
        console.log(props.deviceData)
    }

    componentDidMount(){
        this.setState({
            data : this.props.deviceData
        })
    }

    render() {
        var data = this.state.data.length > 0 ? this.state.data : this.props.deviceData ;
        data.sort((a, b) => {
            return new Date(a.timeStamp).getTime() - new Date(b.timeStamp).getTime();
        });                                                                             //sort by timestamp
        return (
            <div>
                <h3>{this.props.device.name}</h3>
                
                <LineChart
                    width={1500}
                    height={300}
                    data={data}
                    margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
                >
                    <XAxis dataKey="timeStamp" />
                    <YAxis />
                    <CartesianGrid strokeDasharray="3 3" />
                    <Tooltip />
                    <Legend />
                    <Line
                        type="monotone"
                        dataKey="first"
                        stroke="#8884d8"
                        strokeWidth={4}
                    />
                    <Line
                        type="monotone"
                        dataKey="second"
                        stroke="#fda600"
                        strokeWidth={4}
                    />
                    <Line
                        type="monotone"
                        dataKey="third"
                        stroke="#9f2828"
                        strokeWidth={4}
                    />
                </LineChart>
            </div>
        );
    }

    /*constructor(props) {
    super(props);
    this.state = {
            data : this.props.deviceData,
            left : 'dataMin',
            right : 'dataMax',
            refAreaLeft : '',
            refAreaRight : '',
            top : 'dataMax',
            bottom : 'dataMin',
            // top2 : 'dataMax+1',
            // bottom2 : 'dataMin-1',
            // top3 : 'dataMax+1',
            // bottom3 : 'dataMin-1',
            animation : true
        };
    }
  
    getAxisYDomain = (from, to, ref, offset) => {

        const refData = this.state.data.slice(from, to + 1);
        let [ bottom, top ] = [ refData[0][ref], refData[0][ref] ];
        refData.forEach( d => {
            if ( d[ref] > top ) top = d[ref];
            if ( d[ref] < bottom ) bottom = d[ref];
        });
    
    return [ (bottom|0) - offset, (top|0) + offset ]
};

  zoom(){  
  	let { refAreaLeft, refAreaRight, data } = this.state;

    if ( refAreaLeft === refAreaRight || refAreaRight === '' ) {
        this.setState( () => ({
            refAreaLeft : '',
            refAreaRight : ''
        }) );
        return;
    }

		// xAxis domain
    if ( refAreaLeft > refAreaRight ) 
        [ refAreaLeft, refAreaRight ] = [ refAreaRight, refAreaLeft ];

		// yAxis domain
    const [ bottom, top ] = this.getAxisYDomain( refAreaLeft, refAreaRight, 'third', 0 );
    // const [ bottom2, top2 ] = this.getAxisYDomain( refAreaLeft, refAreaRight, 'second', 1 );
    // const [ bottom3, top3 ] = this.getAxisYDomain(refAreaLeft, refAreaRight, 'third', 1);

    this.setState( () => ({
        refAreaLeft : '',
        refAreaRight : '',
        data : data.slice(),
        left : refAreaLeft,
        right : refAreaRight,
        bottom, top,
    //    bottom2, top2, bottom3, top3
    } ) );
  };

    zoomOut() {
        const { data } = this.state;
        this.setState( () => ({
            data : data.slice(),
            refAreaLeft : '',
            refAreaRight : '',
            left : 'dataMin',
            right : 'dataMax',
            top : 'dataMax',
            bottom : 'dataMin',
        //   top2 : 'dataMax+1',
        //   bottom2 : 'dataMin-1',
        //   bottom3 : 'dataMin-1',
        //   top3 : 'dataMin+1'
        }) );
    }

    render() {
        //const data = this.props.deviceData;
        const { data, barIndex, left, right, refAreaLeft, refAreaRight, top, bottom, top2, bottom2, bottom3, top3 } = this.state;
        data.sort((a, b) => {
            return new Date(a.timeStamp).getTime() - new Date(b.timeStamp).getTime();
        }); 
        data.forEach((element, key) => {
            element["name"] = key
        });

        return (
        <div className="highlight-bar-charts">
            <a
            href="javascript: void(0);"
            className="btn update"
            onClick={this.zoomOut.bind( this )}
            >
            Zoom Out
            </a>


        <p>Highlight / Zoom - able Line Chart</p>
          <LineChart
            width={800}
            height={400}
            data={data}
            onMouseDown = { (e) => {
                this.setState({refAreaLeft:e.activePayload[0].payload.name}) }}
            onMouseMove = { (e) => this.state.refAreaLeft && this.setState({refAreaRight:e.activePayload[0].payload.name}) }
            onMouseUp = { this.zoom.bind( this ) }
          >
            <CartesianGrid strokeDasharray="3 3"/>
            <XAxis 
              allowDataOverflow={true}
              dataKey="name"
              domain={[left, right]}
            />
            <YAxis 
              allowDataOverflow={true}
              domain={[bottom, top]}
              type="number"
              yAxisId="1"
             />
            <Tooltip content={<CustomToolTip/>}/>
            <Line yAxisId="1" type='monotone' dataKey='first' stroke="#8884d8" strokeWidth={4} animationDuration={300}/>
            <Line yAxisId="1" type='monotone' dataKey='second' stroke="#fda600" strokeWidth={4} animationDuration={300}/>
            <Line yAxisId="1" type='monotone' dataKey='third' stroke="#9f2828" strokeWidth={4} animationDuration={300}/>
            {
            	(refAreaLeft && refAreaRight) ? (
              <ReferenceArea yAxisId="1" x1={refAreaLeft} x2={refAreaRight}  strokeOpacity={0.3} /> ) : null
            
            }
            
          </LineChart> 

      </div>
        );
  }*/
}

class CustomToolTip extends React.Component<any,any>{

    getIntroOfPage(label) {
        return label
    }

    render(){
        const {active} = this.props
        if (active) {
            const { payload, label } = this.props;
            return (
              <div className="custom-tooltip">
                <p className="label">{`${label} : ${payload[0].payload.timeStamp}`}</p>
                <p className="intro">{this.getIntroOfPage(label)}</p>
                <p className="desc">Anything you want can be displayed here.</p>
              </div>
            );
          }
      
          return null;
    }

}
