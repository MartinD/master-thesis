export const Status = {
    LOADING : "LOADING",
    FETCHED : "FETCHED",
    ERROR : "ERROR"
}