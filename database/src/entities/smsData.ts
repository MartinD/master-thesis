import { create } from "../tests/db";

create("31", {
    id: "31",
    timeStamp: "2018/02/25 23:55",
    first: 1,
    second : 1,
    third : 1,
    smsType: "alert",
    deviceKey: "998",
    type: "data"
});
create("34", {
    id: "34",
    timeStamp: "2018/02/25 22:55",
    first: 0,
    second : 0,
    third : 0,
    smsType: "alert",
    deviceKey: "998",
    type: "data"
});
create("32", {
    id: "32",
    timeStamp: "2018/02/25 23:55",
    first: 1,
    second : 1,
    third : 1,
    smsType: "report",
    deviceKey: "999",
    type: "data"
});
create("35", {
    id: "35",
    timeStamp: "2018/02/25 22:50",
    first: 0,
    second : 0,
    third : 0,
    smsType: "report",
    deviceKey: "999",
    type: "data"
});
create("36", {
    id: "36",
    timeStamp: "2018/01/25 23:50",
    first: 0,
    second : 0,
    third : 0,
    smsType: "report",
    deviceKey: "999",
    type: "data"
});

create("37", {
    id: "37",
    timeStamp: "2018/01/25 23:50",
    first: 0,
    second : 0,
    third : 0,
    smsType: "report",
    deviceKey: "997",
    type: "data"
});
/*create("37", {
    id: "37",
    timeStamp: "25/02/2018 22:55",
    first: 1,
    second : 1,
    third : 1,
    smsType: "report",
    type: "data"
});
create("33", {
    id: "33",
    timeStamp: "25/02/2018 23:55",
    first: {
        cpu: "80%",
        ram: "80%",
        disk: "80%"
    },
    smsType: "report",
    type: "data"
});*/
