import { Actions } from "../actions/Actions";

const initialState = {
    monitoring : []
};

export default (state = initialState, action = { type: "", monitoring : [], status : "" }) => {
    switch (action.type) {
        case Actions.SET_MONITORED_CENTERS:
            return {
                monitoring : action.monitoring,
                status : action.status
            };
        default:
            return state;
    }
};
