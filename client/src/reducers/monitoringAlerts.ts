import { Actions } from "../actions/Actions"

const initialState = {
    data : [],
    status : ""
}

export default (state = initialState, action = { type: "", data : [], status : "" }) => {
    switch (action.type) {
        case Actions.SET_MONITORING_ALERTS:
            return {
                data : action.data,
                status : action.status
            };
        default:
            return state;
    }
};