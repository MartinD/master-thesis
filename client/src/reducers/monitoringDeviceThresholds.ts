import { Actions } from "../actions/Actions"

const initialState = {
    monitoringDeviceThresholds : []
}

export default (state = initialState, action = { type: "", monitoringDeviceThresholds : [], status : "" }) => {
    switch (action.type) {
        case Actions.SET_MONITORING_DEVICE_THRESHOLDS:
            return {
                monitoringDeviceThresholds : action.monitoringDeviceThresholds,
                status : action.status
            };
        default:
            return state;
    }
};