"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("dotenv").config();
const client = require("twilio")(process.env.SMS_SID, process.env.SMS_TOKEN);
const defaultCallBack = (err, msg) => {
    if (!err)
        console.log(msg.sid);
    else
        console.log(err);
};
exports.sendSmsTwilio = (to, from, body, callback = defaultCallBack) => {
    client.messages.create({
        to,
        from,
        body
    }, callback);
};
