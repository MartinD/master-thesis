import * as React from "react"
import { Route, Switch, Redirect } from "react-router-dom"
import {ProfilePage} from "./pages/ProfilePage"
import {SubscriptionsPage} from "./pages/SubscriptionsPage"
import {SubscriptionsData} from "./pages/SubscriptionsData"


export class Routes extends React.Component<any, any>{

    render(){
        return(
            <Switch>
                <Redirect exact from="/settings" to="/settings/profile"/>
                <Route path="/settings/profile" component={ProfilePage}/>
                <Route path="/settings/subscriptions" component={SubscriptionsPage}/>
            </Switch>
        )
    }

}