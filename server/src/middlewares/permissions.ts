export default (req, res, next) => {
    const user = req.currentUser

    if(user.permissions == "admin" || user.permissions == "root"){
        next()
    } else {
        res.status(401).json({ error : "No such permission" })
    }
};
