"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("dotenv").config();
var _ = require("lodash");
const jwt = require("jsonwebtoken");
const db_1 = require("../models/db");
exports.default = (req, res, next) => {
    const authorizationHeader = req.headers["authorization"];
    let token = null;
    if (authorizationHeader) {
        token = authorizationHeader.split(" ")[1];
    }
    if (token) {
        jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
            if (err) {
                res.status(401).json({ error: "Failed to authenticate" });
            }
            else {
                const query = `SELECT CERHIS_MONITOR.* FROM CERHIS_MONITOR WHERE id='${decoded.id}'`;
                db_1.httpsQuery(query, (err, user, meta) => {
                    if (_.isEmpty(user)) {
                        res.status(404).json({ error: "No such user" });
                    }
                    else {
                        req.currentUser = user[0];
                        next();
                    }
                });
            }
        });
    }
    else {
        res.status(403).json({
            error: "No token provided"
        });
    }
};
