import axios from "axios";
import { Actions } from "./Actions";
import { Status } from "./Status";
import monitoringDeviceUsers from "../reducers/monitoringDeviceUsers";
import { stat } from "fs";

export function fetchAllMonitoredCenters() {
    return dispatch => {
        dispatch({
            type: Actions.SET_MONITORED_CENTERS,
            monitoring: [],
            status: Status.LOADING
        });
        return axios
            .get("/api/monitoring/fetchAll")
            .then(res => {
                dispatch({
                    type: Actions.SET_MONITORED_CENTERS,
                    monitoring: res.data.monitoring,
                    status: Status.FETCHED
                });
            })
            .catch(err => {
                dispatch({
                    type: Actions.SET_MONITORED_CENTERS,
                    monitoring: [],
                    status: Status.ERROR
                });
            });
    };
}

export function fetchMonitoredCenters(data) {
    return dispatch => {
        dispatch({
            type: Actions.SET_MONITORED_CENTERS,
            monitoring: [],
            status: Status.LOADING
        });
        return axios
            .post("/api/monitoring/fetch", data)
            .then(res => {
                dispatch({
                    type: Actions.SET_MONITORED_CENTERS,
                    monitoring: res.data.monitoring,
                    status: Status.FETCHED
                });
            })
            .catch(err => {
                dispatch({
                    type: Actions.SET_MONITORED_CENTERS,
                    monitoring: [],
                    status: Status.ERROR
                });
            });
    };
}

export function findMonitoredCenterById(monitoringId) {
    return dispatch => {
        dispatch({
            type: Actions.SET_MONITORED_CENTERS,
            monitoring: {},
            status: Status.LOADING
        });
        return axios
            .get("/api/monitoring/findById/" + monitoringId)
            .then(res => {
                dispatch({
                    type: Actions.SET_MONITORED_CENTERS,
                    monitoring: res.data.monitoring,
                    status: Status.FETCHED
                });
            })
            .catch(err => {
                dispatch({
                    type: Actions.SET_MONITORED_CENTERS,
                    monitoring: {},
                    status: Status.ERROR
                });
            });
    };
}

export function fetchThresholdByDevice(data) {
    return dispatch => {
        dispatch({
            type: Actions.SET_MONITORING_DEVICE_THRESHOLDS,
            monitoringDeviceThresholds: [],
            status: Status.LOADING
        });
        return axios
            .post("/api/monitoring/device/thresholds/fetch", data)
            .then(res => {
                dispatch({
                    type: Actions.SET_MONITORING_DEVICE_THRESHOLDS,
                    monitoringDeviceThresholds:
                        res.data.monitoringDeviceThresholds,
                    status: Status.FETCHED
                });
            })
            .catch(err => {
                console.log(err);
                dispatch({
                    type: Actions.SET_MONITORING_DEVICE_THRESHOLDS,
                    monitoringDeviceThresholds: [],
                    status: Status.ERROR
                });
            });
    };
}

export function fetchCheckTimeByDevice(data) {
    return dispatch => {
        dispatch({
            type: Actions.SET_MONITORING_DEVICE_CHECKTIMES,
            monitoringDeviceCheckTimes: [],
            status: Status.LOADING
        });
        return axios
            .post("/api/monitoring/device/checkTimes/fetch", data)
            .then(res => {
                dispatch({
                    type: Actions.SET_MONITORING_DEVICE_CHECKTIMES,
                    monitoringDeviceCheckTimes:
                        res.data.monitoringDeviceCheckTimes,
                    status: Status.FETCHED
                });
            })
            .catch(err => {
                console.log(err);
                dispatch({
                    type: Actions.SET_MONITORING_DEVICE_CHECKTIMES,
                    monitoringDeviceCheckTimes: [],
                    status: Status.ERROR
                });
            });
    };
}

export function fetchConfigByDevice(data) {
    return dispatch => {
        dispatch({
            type: Actions.SET_MONITORING_DEVICE_CONFIGS,
            monitoringDeviceConfigs: [],
            status: Status.LOADING
        });
        return axios
            .post("/api/monitoring/device/configs/fetch", data)
            .then(res => {
                dispatch({
                    type: Actions.SET_MONITORING_DEVICE_CONFIGS,
                    monitoringDeviceConfigs: res.data.monitoringDeviceConfigs,
                    status: Status.FETCHED
                });
            })
            .catch(err => {
                console.log(err);
                dispatch({
                    type: Actions.SET_MONITORING_DEVICE_CONFIGS,
                    monitoringDeviceConfigs: [],
                    status: Status.ERROR
                });
            });
    };
}

export function fetchDeviceDataByDevice(data) {
    return dispatch => {
        dispatch({
            type: Actions.SET_MONITORING_DEVICE_DATA,
            data: [],
            status: Status.LOADING
        });
        return axios
            .post("/api/monitoring/device/data/fetch", data)
            .then(res => {
                dispatch({
                    type: Actions.SET_MONITORING_DEVICE_DATA,
                    data: res.data.monitoringDeviceData,
                    status: Status.FETCHED
                });
            })
            .catch(err => {
                console.log(err);
                dispatch({
                    type: Actions.SET_MONITORING_DEVICE_DATA,
                    data: [],
                    status: Status.ERROR
                });
            });
    };
}

export function fetchDevicesByCenter(data) {
    return dispatch => {
        dispatch({
            type: Actions.SET_MONITORING_DEVICES,
            data: [],
            status: Status.LOADING
        });
        return axios
            .post("/api/monitoring/devices/fetch", data)
            .then(res => {
                dispatch({
                    type: Actions.SET_MONITORING_DEVICES,
                    data: res.data.monitoringDevices,
                    status: Status.FETCHED
                });
            })
            .catch(err => {
                dispatch({
                    type: Actions.SET_MONITORING_DEVICES,
                    data: [],
                    status: Status.ERROR
                });
            });
    };
}

export function fetchAllDevicesByCenter(data) {
    return dispatch => {
        dispatch({
            type: Actions.SET_MONITORING_DEVICES,
            data: [],
            status: Status.LOADING
        });
        return axios
            .post("/api/monitoring/devices/fetchAll", data)
            .then(res => {
                dispatch({
                    type: Actions.SET_MONITORING_DEVICES,
                    data: res.data.monitoringDevices,
                    status: Status.FETCHED
                });
            })
            .catch(err => {
                dispatch({
                    type: Actions.SET_MONITORING_DEVICES,
                    data: [],
                    status: Status.ERROR
                });
            });
    };
}

export function fetchDeviceUsersByUser(data) {
    return dispatch => {
        dispatch({
            type: Actions.SET_MONITORING_DEVICE_USERS,
            data: [],
            status: Status.LOADING
        });
        return axios
            .post("api/monitoring/devicesUsers/fetch", data)
            .then(res => {
                dispatch({
                    type: Actions.SET_MONITORING_DEVICE_USERS,
                    data: res.data.monitoringDeviceUsers,
                    status: Status.FETCHED
                });
            })
            .catch(err => {
                dispatch({
                    type: Actions.SET_MONITORING_DEVICE_USERS,
                    data: [],
                    status: Status.ERROR
                });
                throw err;
            });
    };
}

export function updateDeviceSubscriptions(data) {
    return dispatch => {
        dispatch({
            type: Actions.SET_MONITORING_DEVICE_USERS,
            data: [],
            status: Status.LOADING
        });
        return axios
            .post("api/monitoring/devices/updateUsers", data)
            .then(res => {
                dispatch({
                    type: Actions.SET_MONITORING_DEVICE_USERS,
                    status: Status.FETCHED,
                    data: res.data.monitoringDeviceUsers
                });
            })
            .catch(err => {
                dispatch({
                    type: Actions.SET_MONITORING_DEVICE_USERS,
                    data: [],
                    status: Status.ERROR
                });
                throw err;
            });
    };
}

export function updateUsersSubscriptions(data) {
    return dispatch => {
        dispatch({
            type: Actions.SET_MONITORING_USERS,
            monitoringUsers: [],
            status: Status.LOADING
        });
        return axios
            .post("api/monitoring/updateUsers", data)
            .then(res => {
                dispatch({
                    type: Actions.SET_MONITORING_USERS,
                    status: Status.FETCHED,
                    monitoringUsers: res.data.monitoringUsers
                });
            })
            .catch(err => {
                dispatch({
                    type: Actions.SET_MONITORING_USERS,
                    monitoringUsers: [],
                    status: Status.ERROR
                });
            });
    };
}

export function fetchMonitoringUsers(data) {
    return dispatch => {
        dispatch({
            type: Actions.SET_MONITORING_USERS,
            monitoringUsers: [],
            status: Status.LOADING
        });
        return axios
            .post("api/monitoring/users/fetch", data)
            .then(res => {
                dispatch({
                    type: Actions.SET_MONITORING_USERS,
                    monitoringUsers: res.data.monitoringUsers,
                    status: Status.FETCHED
                });
            })
            .catch(err => {
                dispatch({
                    type: Actions.SET_MONITORING_USERS,
                    monitoringUsers: [],
                    status: Status.ERROR
                });
            });
    };
}

export function fetchMonitoringAlerts(data){
    return dispatch => {
        dispatch({
            type : Actions.SET_MONITORING_ALERTS,
            data : [],
            status : Status.LOADING
        })
        return axios.post("api/monitoring/alerts/fetch", data).then(res => {
            dispatch({
                type : Actions.SET_MONITORING_ALERTS,
                data : res.data.monitoringAlerts,
                status : Status.FETCHED
            })
        }).catch(err => {
            dispatch({
                type : Actions.SET_MONITORING_ALERTS,
                data : [],
                status : Status.ERROR
            })
        })
    }
}

export function testSMS(data) {
    return dispatch => {
        return axios.post("api/sms", { data }).then(res => {
            console.log("SMS DATA SENT ", data);
        });
    };
}
