import * as React from "react";
import { FormComponentProps } from "antd/lib/form";
import { Switch, Spin, Form, message, Button, Table } from "antd";
import * as uuid from "uuid"

const FormItem = Form.Item;

export interface SubscriptionsFormOwnProps extends FormComponentProps {
    actionSubscribe : (values) => Promise<any>;
    monitoringDevices: any;
    monitoringDeviceUsers: any;
    userKey : string;
    monitoringKey : string;
}

export const SubscriptionsForm = Form.create()(
    class SubscriptionsForm extends React.Component<SubscriptionsFormOwnProps,
        any
    > {
        state = {
            isLoading: false,
            monitoringDeviceUsers : this.props.monitoringDeviceUsers,
            errors: {},
            selectedRowKeys : this.props.monitoringDeviceUsers.map(data => data.deviceKey),
            monitoringKey : 0
        };

        handleSubmit = e => {
            // très important d'en faire un thunk autrement le props form n'est pas reconnu (voir applymiddleware dans index.tsx)
            e.preventDefault();
            this.props.form.validateFields((err, values) => {
                if (!err) {
                    this.setState({
                        isLoading: true,
                        errors: {}
                    });
                    this.props.actionSubscribe(this.createOrUpdate(values)).then(res => {
                        message.success("Données envoyées"); //TODO changer ceci pour éviter que l'utilisateur croient que les données sont enregistrées alors que le serveur n'a pas répondu
                        this.setState({
                            isLoading : false,
                            errors : {}
                        });
                    }).catch(err => {
                        this.setState({
                            isLoading : false,
                            errors : err
                        });
                        message.error("Erreur lors de l'envoi des données");
                    })
                    // this.props
                    //     .actionProfile(values)
                    //     .then(res => {
                    //         this.setState({
                    //             isLoading: false,
                    //             errors: {}
                    //         });
                    //         message.success("Données enregistrées");
                    //     })
                    //     .catch(err => {
                    //         this.setState({
                    //             isLoading: false,
                    //             errors: err.response.data.errors
                    //         });
                    //         message.error("Erreur lors de l'enregistrement");
                    //     });
                }
            });
        };

        updateSubscriptions = e => {
            e.preventDefault();
            console.log("Selected row keys: ", this.state.selectedRowKeys);
            console.log("Monitoring Device Users: ", this.state.monitoringDeviceUsers);
            this.props.actionSubscribe({
                monitoringKey : this.state.monitoringKey
            })
            // this.setState({
            //     isLoading: true,
            //     errors: {}
            // });
            // this.props.actionSubscribe(this.createOrUpdate(values)).then(res => {
            //     message.success("Données envoyées"); //TODO changer ceci pour éviter que l'utilisateur croient que les données sont enregistrées alors que le serveur n'a pas répondu
            //     this.setState({
            //         isLoading : false,
            //         errors : {}
            //     });
            // }).catch(err => {
            //     this.setState({
            //         isLoading : false,
            //         errors : err
            //     });
            //     message.error("Erreur lors de l'envoi des données");
            // })
        }

        isDeviceInDeviceUsers(device){
            for(var i=0; i<this.props.monitoringDeviceUsers.length; i++){
                if(device.id == this.props.monitoringDeviceUsers[i].deviceKey){
                    return true
                }       
            }
            return false
        }

        createOrUpdate(values){
            const monitoringDevices = this.props.monitoringDevices
            var createDeviceUsers = []
            var deleteDeviceUsers = []
            for(var i=0; i<monitoringDevices.length; i++){
                if(values[monitoringDevices[i].id] == true && !this.isDeviceInDeviceUsers(monitoringDevices[i])){
                    createDeviceUsers.push(
                        {
                            deviceKey : monitoringDevices[i].id,
                            userKey : this.props.userKey,
                        }
                    )
                }
                else if(values[monitoringDevices[i].id] == false && this.isDeviceInDeviceUsers(monitoringDevices[i])){
                    deleteDeviceUsers.push( this.userDeviceKeyFromDeviceKey(monitoringDevices[i].id) )
                }
            }
            return {createDeviceUsers, deleteDeviceUsers}
        }

        userDeviceKeyFromDeviceKey(deviceKey){
            for(var i=0; i<this.props.monitoringDeviceUsers.length; i++){
                if(deviceKey == this.props.monitoringDeviceUsers[i].deviceKey){
                    return this.props.monitoringDeviceUsers[i].id
                }       
            }
            return 10000
        }


        render() {
            const columns = [
                {
                    title : "Nom",
                    dataIndex : "name",
                    key : "name"
                },
                {
                    title : "Adresse IP",
                    dataIndex : "ip",
                    key : "ip"
                }
            ]
            const data = [];
            const monitoringDevices = this.props.monitoringDevices;
            for(var i=0; i<monitoringDevices.length; i++){
                data.push({
                    key : monitoringDevices[i].id,
                    name : monitoringDevices[i].name,
                    ip : monitoringDevices[i].ip
                })
            }
            const Title = () => {
                return (
                    <span className="devices-table-title">
                        Services auxquels vous êtes abonnés ({this.state.monitoringDeviceUsers.length || 0})
                    </span>
                );
            }
            const rowSelection = {
                onChange : (selectedRowKeys, selectedRows) => {
                    this.setState({selectedRowKeys})
                },
                selectedRowKeys : this.state.selectedRowKeys
            }
            const { getFieldDecorator } = this.props.form;
            const formItemLayout = {
                labelCol: {
                    xs: { span: 18 },
                    sm: { span: 5 }
                },
                wrapperCol: {
                    xs: { span: 18 },
                    sm: { span: 9 }
                }
            };
            
            return (
                <Spin spinning={this.state.isLoading}>
                    <Form onSubmit={this.handleSubmit} className="user-form">
                        {this.props.monitoringDevices.map((device, key) => {
                            return (
                                <FormItem
                                    {...formItemLayout}
                                    label={
                                        device.name + " (" + device.ip + ") "
                                    }
                                    key={key}
                                >
                                    {getFieldDecorator(device.id.toString(), {
                                        valuePropName: "checked",
                                        initialValue:this.isDeviceInDeviceUsers(device)
                                    })(<Switch />)}
                                </FormItem>
                            );
                        })}
                        <FormItem wrapperCol={{ span: 18, offset: 5 }}>
                            <Button
                                type="primary"
                                htmlType="submit"
                                className="login-form-button"
                                style={{ width: "50%" }}
                            >
                                Enregistrer les abonnements
                            </Button>
                        </FormItem>
                    </Form>
                    <Table
                        title={Title}
                        columns={columns}
                        dataSource={data}
                        rowSelection={rowSelection}
                        >
                    </Table>
                    <Button
                        onClick={this.updateSubscriptions}
                        type="primary"
                        htmlType="submit"
                        className="update-subscriptions"
                        style={{ width: "50%" }} >
                        Enregistrer les abonnements
                    </Button>
                </Spin>
            );
        }
    }
);
