require("dotenv").config()
const client = require("twilio")(process.env.SMS_SID, process.env.SMS_TOKEN)

const defaultCallBack = (err, msg) => {
    if(!err) console.log(msg.sid)
    else console.log(err)
}

export const sendSmsTwilio = (to : string, from : string, body : string, callback = defaultCallBack) => {
    client.messages.create(
        {
            to,
            from,
            body
        },
        callback
    )
}