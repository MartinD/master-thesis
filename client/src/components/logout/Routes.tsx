import * as React from "react";
import { HashRouter as Router, Route } from "react-router-dom";
import { LogoutPage } from "./pages/LogoutPage"

export class Routes extends React.Component<any, any> {
    render() {
        return (
            <Router>
                <Route path="/logout" component={LogoutPage}/>
            </Router>
        )
    }
}

