"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require('dotenv').config();
const express = require("express");
const path = require("path");
const bodyParser = require("body-parser");
const users_1 = require("./routers/users");
const auth_1 = require("./routers/auth");
const monitoring_1 = require("./routers/monitoring");
const sms_1 = require("./routers/sms");
var couchbase = require("couchbase");
var cluster = new couchbase.Cluster("couchbase://127.0.0.1");
cluster.authenticate(process.env.COUCHBASE_USERNAME, process.env.COUCHBASE_PASSWORD);
var bucket = cluster.openBucket("CERHIS_MONITOR");
let app = express();
app.use(bodyParser.json()); //data available in request.body
app.use("/api/users", users_1.default);
app.use("/api/auth", auth_1.default);
app.use("/api/monitoring", monitoring_1.default);
app.use("/api/sms", sms_1.default);
app.use(express.static(path.resolve(__dirname, '../../client/build')));
app.get('*', function (req, res) {
    res.sendFile(path.resolve(__dirname, '../../client/build/index.html'));
    res.end();
});
app.listen(8081, () => console.log("Running on localhost:8081"));
