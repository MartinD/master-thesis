import * as React from "react";
import { HashRouter as Router, Switch, Route } from "react-router-dom";
import LoginPage from "./pages/LoginPage";

export class Routes extends React.Component<any, any> {
  render() {
    return (
      <Switch>
        <Route exact path="/login" component={LoginPage} />
      </Switch>
    );
  }
}

export default Routes;
