import { Actions } from "../actions/Actions"

const initialState = {
    monitoringDeviceCheckTimes : []
}

export default (state = initialState, action = { type: "", monitoringDeviceCheckTimes : [], status : "" }) => {
    switch (action.type) {
        case Actions.SET_MONITORING_DEVICE_CHECKTIMES:
            return {
                monitoringDeviceCheckTimes : action.monitoringDeviceCheckTimes,
                status : action.status
            };
        default:
            return state;
    }
};