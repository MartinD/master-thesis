"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SmsFromTwilio = sms => {
    const smsFormated = {
        id: sms.SmsSid,
        body: sms.Body,
        from: sms.From,
        to: sms.To
    };
    return smsFormated;
};
