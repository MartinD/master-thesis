var express = require("express");
var twilio = require("twilio");
import {
    Create,
    CreateMultiple,
    DeleteMultiple,
    UpdateMultiple,
    httpsQuery
} from "../models/db";
import { SmsFromTwilio } from "../models/smsFormat";
import { Actions } from "./actions/actions";
import { sendSmsTwilio } from "../models/sms"
import { CONNREFUSED } from "dns";
import * as _ from "lodash"

let router = express.Router();

router.get("/", (req, res) => {
    var smsFromTwilio = SmsFromTwilio(req.query);
    var smsBody = smsFromTwilio.body;
    smsBody = smsBody.split("(").join("{");    //certains encodages ne permettent pas d'encoder les brackets
    smsBody = smsBody.split(")").join("}");
    const smsBodyParsed = smsBody.split("&&"); //&& = delimiteur utilise dans les SMS
    //const smsBodyParsed = req.body.data.split("&&");    // test
    const action = smsBodyParsed[0];                    
    const data = _.sortBy(smsBodyParsed.splice(1).map(element => {
        return JSON.parse(element)
    }), "deviceKey");                                      // Les data et les device sont tries par deviceKey pour pouvoir les traiter en parallèle dans reportSms()
    switch (action) {                                      // met à jour la base de donnees en fonction de l'action du sms
        case Actions.CREATE:
            CreateMultiple(data);
            break;
        case Actions.UPDATE:
            UpdateMultiple(data);
            break;
        case Actions.DELETE:
            DeleteMultiple(data);
            break;
    }

    let query;                                        //envoie un sms aux technicien lies au service si le type de sms est "data"
    switch(data[0].type) {  
        case "data":
            switch(data[0].smsType){
                case "alert":
                    query = `select c1.* from CERHIS_MONITOR c1 where c1.type="user" and c1.id in 
                    (select raw userKey from CERHIS_MONITOR where type="deviceUser" and deviceKey="${data[0].deviceKey}" )`;
                    break;
                case "noAlert":
                    query = `select c1.* from CERHIS_MONITOR c1 where c1.type="user" and c1.id in 
                    (select raw userKey from CERHIS_MONITOR where type="deviceUser" and deviceKey="${data[0].deviceKey}" )`;
                    break;
                case "report":
                    query = `select c1.* from CERHIS_MONITOR c1 where c1.type="user" and c1.id in
                    (select raw c2.userKey from CERHIS_MONITOR c2 where c2.type="usersMonitoring" and c2.monitoringKey in
                    (select raw monitoringKey from CERHIS_MONITOR where type="device" and id="${data[0].deviceKey}" ) )`
                    break;
            }

            httpsQuery(query, (err, users, meta) => {
                if(!err){
                    users.forEach(user => {
                        if(user.permissions != "responsible"){
                            fromJsonToHumanReadable(data, msg => {
                                console.log("MESSAGE", msg)
                                sendSmsTwilio("+"+user.prefix+user.phone, "+16202134324", msg)
                            })
                        }
                    });
                }
            })
            break;
        default : 
            break;
    }
    res.writeHead(200, {'Content-Type': 'text/xml'});
    res.end();
});

/**
 * Convertit les donnees JSON en un SMS lisible par un humain
 * @param data 
 */
const fromJsonToHumanReadable = (data, callback) => {
    var orSequence = "";
    for(var i=0; i<data.length; i++){
        orSequence += `c1.id="${data[i].deviceKey}" or `;
    }
    orSequence = orSequence.slice(0, -4);
    const query = `select c1.* from CERHIS_MONITOR c1 where c1.type="device" and (${orSequence}) order by c1.id`       // Les data et les device sont tries par deviceKey pour pouvoir les traiter en parallèle dans reportSms
    httpsQuery(query, (err, devices, meta) => {
        let msg;
        if(!err){
            if(data[0].smsType == "alert"){
                const device = devices[0]
                const data1 = data[0]
                msg = alertSms(device, data1)
            } else if(data[0].smsType == "noAlert"){
                const device = devices[0]
                const data1 = data[0]
                msg = noAlertSms(device, data1)
            } else if(data[0].smsType == "report"){
                msg = reportSms(devices, data)
            }
        } else {
            msg = "Erreur sur le serveur web"
        }
        callback(msg)
    })
}

function alertSms(device, data){
    var msg = `Alerte du centre hospitalier ${device.monitoringKey} \n`;
    switch(device.deviceType){
        case 0:
            msg += "Deconnecte du wifi"
            break;
        case 1:
            msg += `${device.ip} (${device.name}) est absent du reseau`
            break;
        case 2:
            msg += `Valeurs du serveur ${device.ip} (${device.name}) :\n CPU => ${data.first}%\n RAM => ${data.second}%\n DISK => ${data.third}`
            break;
        case 3:
            msg += `Charge de ${device.ip} (${device.name}) : ${data.first}%`
            break;
        case 4:
            msg += `Batterie du smartphone : ${data.first}%`
            break;
        case 5:
            msg += `Reseau electrique down`
            break;
        case 6:
            msg += `Voltage des panneaux solaires : ${data.first}%`
            break;
    }
    return msg;
}

function noAlertSms(device, data){
    var msg = `Retour à la normale pour le centre hospitalier ${device.monitoringKey} \n`
    switch(device.deviceType){
        case 0:
            msg += "Connecte au wifi"
            break;
        case 1:
            msg += `${device.ip} (${device.name}) est connecte au reseau`
            break;
        case 2:
            msg += `Valeurs du serveur ${device.ip} (${device.name}) :\n CPU => ${data.first}%\n RAM => ${data.second}%\n DISK => ${data.third}`
            break;
        case 3:
            msg += `Charge de ${device.ip} (${device.name}) : ${data.first}%`
            break;
        case 4:
            msg += `Batterie du smartphone : ${data.first}%`
            break;
        case 5:
            msg += `Reseau electrique operationnel`
            break;
        case 6:
            msg += `Voltage des panneaux solaires : ${data.first}%`
            break;
    }
    return msg;
}

function reportSms(devices, data){
    var msg = `Rapport journalier du centre hospitalier ${devices[0].monitoringKey} \n`
    for(var i=0; i<devices.length; i++){
        switch(devices[i].deviceType){
            case 0:
                msg += (data[i].first == 1) ? "Connecte au wifi\n" : "Deconnecte du wifi\n" ;
                break;
            case 1:
                msg += (data[i].first == 1) ? `${devices[i].ip} (${devices[i].name}) est connecte au reseau\n` : `${devices[i].ip} (${devices[i].name}) est absent du reseau\n` ;
                break;
            case 2:
                msg += `Valeurs du serveur ${devices[i].ip} (${devices[i].name}) :\n CPU => ${data[i].first}%\n RAM => ${data[i].second}%\n DISK => ${data[i].third}\n`
                break;
            case 3:
                msg += `Charge de ${devices[i].ip} (${devices[i].name}) : ${data[i].first}%\n`
                break;
            case 4:
                msg += `Batterie du smartphone : ${data[i].first}%\n`
                break;
            case 5:
                msg += (data[i].first == 1) ? `Reseau electrique operationnel\n` : `Reseau electrique down\n`;
                break;
            case 6:
                msg += `Voltage des panneaux solaires : ${data.first}%\n`
                break;
        }
    }
    msg += "Fin du rapport"
    return msg;
}

export default router;
