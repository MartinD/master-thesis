import * as React from "react";
import { connect } from "react-redux";
import {
    Table,
    Icon,
    Divider,
    Button,
    Spin,
    Modal,
    message,
    Card,
    Steps,
    Popover
} from "antd";
import { withRouter, Redirect, Link } from "react-router-dom";
import {
    fetchAllMonitoredCenters,
    fetchMonitoringUsers,
    fetchAllDevicesByCenter,
    fetchDeviceUsersByUser,
    updateDeviceSubscriptions,
    updateUsersSubscriptions
} from "../../../actions/Monitoring";
import { Status } from "../../../actions/Status";
import { fetchUsers } from "../../../actions/Users";

const Step = Steps.Step;
const customDot = (dot, { status, index, title, description }) => (
    <Popover content={<span>{description}</span>}>
      {dot}
    </Popover>
  );

export const Subscriptions = withRouter(
    connect(
        state => {
            return {
                auth: state.auth,
                monitoring: state.monitoring,
                users: state.users,
                monitoringUsers: state.monitoringUsers,
                monitoringDevices: state.monitoringDevices,
                monitoringDeviceUsers: state.monitoringDeviceUsers
            };
        },
        {
            fetchAllMonitoredCenters,
            fetchUsers,
            fetchMonitoringUsers,
            fetchAllDevicesByCenter,
            fetchDeviceUsersByUser,
            updateDeviceSubscriptions,
            updateUsersSubscriptions
        }
    )(
        class Subscriptions extends React.Component<any, any> {
            state = {
                selectedRowKeys: [],
                selectedRowKeys2: [],
                selectedRowKeys3: [],
                selectedRowKeys4 : [],
                monitoringKey: null,
                monitoringKey2: null,
                userKey: null,
                userKey2: null,
                isLoading: false,
                step: 0,
                step2:0,
                key : "center"
            };

            componentWillMount() {
                this.props.fetchAllMonitoredCenters();
            }

            onTabChange = (key, type) => {
                switch(key){
                    case "center":
                        this.props.fetchAllMonitoredCenters();
                        break;
                    case "user":
                        this.props.fetchUsers(this.props.auth.user);
                        break;
                }
                this.setState({
                    [type] : key
                })
            }

            updateSubscriptions = e => {
                e.preventDefault();
                var dataUsers = {
                    monitoringKey: this.state.key == "center" ? this.state.monitoringKey : this.state.monitoringKey2,
                    monitoringUserKeys: this.state.key == "center" ? this.state.selectedRowKeys : this.state.selectedRowKeys3,
                    userKey: this.state.key == "center" ? null : this.state.userKey2
                };
                var dataUserDevices = {
                    monitoringKey: this.state.key == "center" ? this.state.monitoringKey : this.state.monitoringKey2,
                    monitoringUserKeys: this.state.key == "center" ? this.state.selectedRowKeys : this.state.selectedRowKeys3,
                    monitoringDeviceUserKeys: this.state.key == "center" ? this.state.selectedRowKeys2 : this.state.selectedRowKeys4,
                    userKey: this.state.key == "center" ? this.state.userKey : this.state.userKey2
                };
                this.props
                    .updateUsersSubscriptions(dataUsers)
                    .then(res => {
                        message.success("Abonnements au centre envoyés");
                    })
                    .catch(err => {
                        message.error(
                            "Erreur lors de l'abonnement aux centres"
                        );
                    });
                this.props
                    .updateDeviceSubscriptions(dataUserDevices)
                    .then(res => {
                        this.setState({
                            errors: {}
                        });
                        message.success("Abonnements aux services envoyés");
                    })
                    .catch(err => {
                        this.setState({
                            isLoading: false,
                            errors: err.response.data.errors
                        });
                        message.error(
                            "Erreur lors de l'abonnement aux services"
                        );
                    });
            };

            fetchDeviceFromUserFromCenter = (record) => {
                this.props
                    .fetchAllDevicesByCenter({
                        monitoringKey: this
                            .state.monitoringKey
                    })
                    .then(() => {
                        this.setState({
                            monitoringDevices: this
                                .props
                                .monitoringDevices
                                .data
                        });
                    });
                this.props
                    .fetchDeviceUsersByUser({
                        userKey: record["key"],
                        monitoringKey: this
                            .state.monitoringKey
                    })
                    .then(() => {
                        this.setState({
                            selectedRowKeys2: this.props.monitoringDeviceUsers.data.map(
                                data =>
                                    data.deviceKey
                            ),
                            isLoading: false
                        });
                    });
                this.setState({
                    userKey: record["key"],
                    step: 2
                });
            }

            fetchDeviceFromCenterFromUser = (record) => {
                this.props
                    .fetchAllDevicesByCenter({
                        monitoringKey: record["key"]
                    })
                    .then(() => {
                        this.setState({
                            monitoringDevices: this.props.monitoringDevices.data
                        });
                    });
                this.props.fetchDeviceUsersByUser({
                    userKey : this.state.userKey2,
                    monitoringKey : record["key"]
                })
                .then(() => {
                    this.setState({
                        selectedRowKeys4 : this.props.monitoringDeviceUsers.data.map(
                            data => data.deviceKey
                        )
                    })
                })
                this.setState({
                    monitoringKey2 : record["key"],
                    step2 : 2
                })
            }

            fetchCenterFromUser = (record) => {
                this.props.fetchAllMonitoredCenters();
                this.props
                    .fetchMonitoringUsers({
                        userKey: record["key"]
                    })
                    .then(() => {
                        this.setState({
                            selectedRowKeys3: this.props.monitoringUsers.monitoringUsers.map(
                                data => data.monitoringKey
                            )
                        });
                    });
                this.setState({
                    step2 : 1,
                    userKey2 : record["key"]
                })
            }

            fetchUserFromCenter = (record) => {
                this.props.fetchUsers(
                    this.props.auth.user
                );
                this.props
                    .fetchMonitoringUsers({
                        monitoringKey: record["key"]
                    })
                    .then(() => {
                        this.setState({
                            selectedRowKeys: this.props.monitoringUsers.monitoringUsers.map(
                                data => data.userKey
                            )
                        });
                    });
                this.setState({
                    monitoringKey: record["key"],
                    step: 1
                });
            }

            render() {
                const columns1 = [
                    {
                        title: "Nom",
                        dataIndex: "name",
                        key: "name",
                        //render: (text, record, index) => (
                            //<Link to={`/admin/monitoring-data/${record.key}`}>
                        //        <h3>{text}</h3>
                            //</Link>
                        //)
                    },
                    {
                        title: "Responsable",
                        dataIndex: "responsible",
                        key: "responsible"
                    },
                    {
                        title: "Téléphone",
                        dataIndex: "phone",
                        key: "phone"
                    }
                ];
                const columns2 = [
                    {
                        title: "Nom",
                        dataIndex: "name",
                        key: "name",
                        render: (text, record, index) => (
                            <Link to={`/admin/users-data/${record.key}`}>
                                {text}
                            </Link>
                        )
                    },
                    {
                        title: "Email",
                        dataIndex: "email",
                        key: "email"
                    },
                    {
                        title: "permissions",
                        dataIndex: "permissions",
                        key: "permissions"
                    }
                ];
                const columns3 = [
                    {
                        title: "Nom",
                        dataIndex: "name",
                        key: "name"
                    },
                    {
                        title: "Adresse IP",
                        dataIndex: "ip",
                        key: "ip"
                    },
                    {
                        title : "Type de monitoring",
                        dataIndex : "type",
                        key : "type"
                    }
                ];
                const rowSelection = {
                    onChange: (selectedRowKeys, selectedRows) => {
                        this.setState({ selectedRowKeys });
                    },
                    getCheckboxProps: record => ({
                        disabled: record.name === "Disabled User", // Column configuration not to be checked
                        name: record.name
                    }),
                    selectedRowKeys: this.state.selectedRowKeys
                };
                const rowSelection2 = {
                    onChange: (selectedRowKeys2, selectedRows) => {
                        this.setState({ selectedRowKeys2 });
                    },
                    getCheckboxProps: record => ({
                        disabled: record.name === "Disabled User", // Column configuration not to be checked
                        name: record.name
                    }),
                    selectedRowKeys: this.state.selectedRowKeys2
                };
                const rowSelection3 = {
                    onChange: (selectedRowKeys3, selectedRows) => {
                        this.setState({ selectedRowKeys3 });
                    },
                    getCheckboxProps: record => ({
                        disabled: record.name === "Disabled User", // Column configuration not to be checked
                        name: record.name
                    }),
                    selectedRowKeys: this.state.selectedRowKeys3
                }
                const rowSelection4 = {
                    onChange: (selectedRowKeys4, selectedRows) => {
                        this.setState({ selectedRowKeys4 });
                    },
                    getCheckboxProps: record => ({
                        disabled: record.name === "Disabled User", // Column configuration not to be checked
                        name: record.name
                    }),
                    selectedRowKeys: this.state.selectedRowKeys4
                }
                const data = [];
                const monitoring = this.props.monitoring.monitoring;
                for (var i = 0; i < monitoring.length; i++) {
                    data.push({
                        key: monitoring[i].id,
                        name: monitoring[i].name,
                        responsible: monitoring[i].responsible,
                        phone: monitoring[i].phone
                    });
                }
                const data2 = [];
                const users = this.props.users.users;
                for (var i = 0; i < users.length; i++) {
                    data2.push({
                        key: users[i].id,
                        name: users[i].firstname + " " + users[i].name,
                        email: users[i].email,
                        permissions: users[i].permissions
                    });
                }
                const data3 = [];
                const monitoringDevices = this.props.monitoringDevices.data;
                for (var i = 0; i < monitoringDevices.length; i++) {
                    data3.push({
                        key: monitoringDevices[i].id,
                        name: monitoringDevices[i].name,
                        ip: monitoringDevices[i].ip,
                        type : monitoringDevices[i].deviceType
                    });
                }
                const Title = () => {
                    return (
                        <span className="monitoring-table-title">
                            Liste de tous les centres de monitoring ({this.props
                                .monitoring.monitoring.length || 0})
                        </span>
                    );
                };
                const Title2 = () => {
                    return (
                        <span className="user-table-title">
                            Liste des utilisateurs abonnés au centre ({this.props.monitoringUsers.monitoringUsers.length || 0})
                        </span>
                    );
                };
                const Title3 = () => {
                    return (
                        <span className="devices-table-title">
                            Services auxquels l'utilisateur est abonné ({this.props.monitoringDeviceUsers.data.length || 0})
                        </span>
                    );
                };
                const Title4 = () => {
                    return (
                        <span className="users-table-title">
                            Liste de tous les utilisateurs ({this.props.users.users.length || 0})
                        </span>
                    );
                }
                const Title5 = () => {
                    return (
                        <span className="users-table-title">
                            Liste de tous les centre auxquels l'utilisateur est abonné ({this.props.monitoringUsers.monitoringUsers.length || 0})
                        </span>
                    );
                }      
                const usersTable = (
                    <Spin
                        spinning={this.props.users.status === "LOADING"}
                    >
                        {/* <p>En abonnant un utilisateur à un centre de santé, cette personne 
                            recevra quotidiennement les SMS de rapport permettant d'être 
                            tenu au courant du fonctionnement global du système. De plus,
                            être abonné à un centre de santé donne le droit de visualiser 
                            ses données via l'onglet <b>Surveillance</b>
                        </p> */}
                        <Table
                            title={this.state.key == "center" ? Title2 : Title4}
                            columns={columns2}
                            dataSource={data2}
                            rowSelection={this.state.key == "center" ? rowSelection : null}
                            onRow={(record: Object) => {
                                return {
                                    onClick: () => {
                                        switch(this.state.key){
                                            case "center":
                                                this.fetchDeviceFromUserFromCenter(record)
                                                break;
                                            case "user":
                                                this.fetchCenterFromUser(record)
                                                break;
                                        }
                                    }
                                };
                            }}
                        />
                    </Spin>
                )
                const deviceTable = (
                    <Spin
                        spinning={
                            this.props.monitoringDevices.status ==
                            Status.LOADING
                        }
                    >
                        {/* <p>
                            En abonnant un utilisateur à un service, cette personne recevra 
                            tous les SMS d'alerte et de retour à la normale concernant l'état
                            du service dans le centre de santé. Attention! Vous devez abonner un 
                            utilisateur à un centre de santé pour pouvoir l'abonner à un service. 
                        </p> */}
                        <Table
                            title={Title3}
                            columns={columns3}
                            dataSource={data3}
                            rowSelection={this.state.key == "center" ? rowSelection2 : rowSelection4}
                        />
                    </Spin>
                )
                const monitoringTable = (
                    <Table
                        title={this.state.key == "center" ? Title : Title5}
                        columns={columns1}
                        dataSource={data}
                        rowSelection={this.state.key == "user" ? rowSelection3 : null}
                        onRow={(record: Object) => {
                            return {
                                onClick: () => {
                                    switch(this.state.key){
                                        case "center":
                                            this.fetchUserFromCenter(record);
                                            break;
                                        case "user":
                                            this.fetchDeviceFromCenterFromUser(record);
                                            break;
                                    }
                                }
                            };
                        }}
                    />
                )

                const tabList = [{
                    key : "center",
                    tab : "A partir des centres"
                },{
                    key : "user",
                    tab : "A partir des utilisateurs"
                }]
    
                const contentList = {
                    center : (
                    <Spin spinning={this.props.monitoring.status === "LOADING"}>
                        <Steps current={this.state.step} progressDot={customDot}>
                            <Step
                                title="Centres"
                                description="Cliquez sur un centre de santé."
                            />
                            <Step
                                title="Utilisateurs"
                                description="Cliquez sur un utilisateur."
                            />
                            <Step
                                title="Services"
                                description="Sélectionnez les services."
                            />
                        </Steps>
                        {this.state.step >= 0 ? monitoringTable : null}
                        {this.state.step >= 1 ? usersTable : null}
                        {this.state.step >= 2 ? deviceTable : null}
                        <Button
                            disabled={this.state.step != 2}
                            onClick={this.updateSubscriptions}
                            type="primary"
                            htmlType="submit"
                            className="update-subscriptions"
                            style={{ width: "100%" }}
                        >
                            Enregistrer les abonnements
                        </Button>
                    </Spin>),
                    user :  (
                    <Spin spinning={this.props.users.status === "LOADING"}>
                        <Steps current={this.state.step2} progressDot={customDot}>
                            <Step
                                title="Utilisateurs"
                                description="Cliquez sur un utilisateur."
                            />
                            <Step
                                title="Centres"
                                description="Cliquez sur un centre de santé."
                            />
                            <Step
                                title="Services"
                                description="Sélectionnez les services."
                            />
                        </Steps>
                        {this.state.step2 >= 0 ? usersTable : null}
                        {this.state.step2 >= 1 ? monitoringTable : null}
                        {this.state.step2 >= 2 ? deviceTable : null}
                        <Button
                            disabled={this.state.key == "center" ? this.state.step != 2 : this.state.step2 != 2}
                            onClick={this.updateSubscriptions}
                            type="primary"
                            htmlType="submit"
                            className="update-subscriptions"
                            style={{ width: "100%" }}
                        >
                            Enregistrer les abonnements
                        </Button>
                    </Spin>)
                }
                return (
                    <Card title="Abonnements" tabList={tabList} onTabChange={(key) => {this.onTabChange(key, "key")}}>
                        {contentList[this.state.key]}
                    </Card>
                );
            }
        }
    )
);
