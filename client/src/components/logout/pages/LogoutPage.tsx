import * as React from "react";
import { connect, DispatchProp } from "react-redux";
import { Redirect } from "react-router-dom";
import { userLogoutRequest } from "../../../actions/Auth";

export interface LogoutPageOwnProps {
    auth: any;
    userLogoutRequest: () => void;
}

export const LogoutPage = connect(state => {
    return {
        auth: state.auth
    };
})(
    class LogoutPage extends React.Component<LogoutPageOwnProps & DispatchProp<any>,any> {
        render() {
            this.props.dispatch(userLogoutRequest());
            return <Redirect to="/login" />;
        }
    }
);

export default LogoutPage;
