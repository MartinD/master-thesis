import * as React from "react";
import { Card } from "antd";
import { FormComponentProps } from "antd/lib/form";
import { connect, DispatchProp } from "react-redux";
import { updateProfile } from "../../../actions/Users";
import { countryPrefix } from "../../../resources/CountryPrefix";
import { UserForm } from "../../shared-components/UserForm";

export const ProfilePage = connect(
    state => {
        return {
            auth: state.auth
        };
    },
    { updateProfile }
)(
    class ProfilePage extends React.Component<any, any> {
        render() {
            return (
                <Card title={"Vos données personnelles"} style={{ maxWidth : 800, margin : "auto" }}>
                    <UserForm
                        actionProfile={this.props.updateProfile}
                        user={this.props.auth.user}
                        auth={this.props.auth.user}
                    />
                </Card>
            );
        }
    }
);

export default ProfilePage;
