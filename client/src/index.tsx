import * as React from "react";
import * as ReactDOM from "react-dom";
import {
    HashRouter as Router,
    Redirect,
    Switch,
    Route,
    Link,
    NavLink
} from "react-router-dom";
import { Provider } from "react-redux";
import thunk from "redux-thunk";
import { createStore, applyMiddleware, compose, Store } from "redux";
import * as jwt from "jsonwebtoken";

import rootReducer from "./rootReducer";
import setAuthorizationToken from "./utils/setAuthorizationToken";
import { Actions } from "./actions/Actions";
import { Status } from "./actions/Status";

import "antd/dist/antd.less";
import "../style/index.less";

import { Routes } from "./components/Routes";

const enhancer = window["devToolsExtension"]
    ? window["devToolsExtension"]()(createStore)
    : createStore;

const store = createStore(
    rootReducer,
    compose(
        applyMiddleware(thunk), //allow to dispatch asynchronous actions
        window["devToolsExtension"] ? window["devToolsExtension"]() : f => f
    )
);

if (localStorage.jwtToken) {
    setAuthorizationToken(localStorage.jwtToken); // sinon on perd le token précédemment enregistré
    store.dispatch({
        type: Actions.SET_CURRENT_USER,
        user: jwt.decode(localStorage.jwtToken),
        status: Status.FETCHED
    });
}

ReactDOM.render(
    <Provider store={store}>
        <Routes />
    </Provider>,
    document.getElementById("mount")
);
