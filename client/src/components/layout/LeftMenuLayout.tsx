import * as React from "react";
import { Layout, Menu, Breadcrumb, Icon, Modal, Button } from "antd";
import { SiderProps } from "antd/lib/layout";
import {
    NavLink,
    Link,
    Switch,
    Route,
    withRouter,
    RouteComponentProps
} from "react-router-dom";
import { connect, DispatchProp } from "react-redux";

const { Header, Content, Footer, Sider } = Layout;
const SubMenu = Menu.SubMenu;
const Item = Menu.Item;

export interface LeftSiderOwnProps extends SiderProps {
    auth?: any;
}

export const LeftMenuLayout = withRouter(
    connect<any, any, LeftSiderOwnProps & RouteComponentProps<any>>(state => {
        return {
            auth: state.auth
        };
    })(
        class LeftSider extends React.Component<LeftSiderOwnProps, any> {
            constructor(props) {
                super(props);
                this.state = {
                    collapsed: false
                };
            }

            onCollapse = collapsed => {
                this.setState({ collapsed });
            };

            reduceName(name: string) {
                return name
                    .split(" ")
                    .slice(0, 2)
                    .map(s => s[0])
                    .join(".")
                    .toUpperCase();
            }

            info() {
                Modal.info({
                  title: "Informations sur l'application",
                  content: (
                    <div>
                      <p>Cette application a été réalisée dans le cadre du mémoire de 
                          fin d'études de Martin Delobbe à l'école polytechnique de 
                          Bruxelles en vue de l'obtention du grade d'ingénieur informatitien</p>
                      <p>Pour toutes questions concernant l'application et sa maintenance,
                          contactez directement l'entreprise <b>AEDES</b> dont le site est accessible
                          en cliquant sur le logo en haut à gauche de l'application.
                      </p>
                    </div>
                  ),
                  onOk() {},
                });
              }

            render() {
                const user = this.props.auth.user || {
                    firstname: "John",
                    name: "Doe",
                    permissions: "admin"
                };
                let backgroundImage = null;
                if (user.permissions == "root")
                    backgroundImage =
                        "/assets/square-root-mathematical-symbol.png";
                else if (user.permissions == "admin")
                    backgroundImage = "/assets/man-with-tie.png";
                else if (user.permissions == "responsible")
                    backgroundImage = "/assets/organization.png";
                else backgroundImage = "/assets/tool-cross.png";
                const name = this.state.collapsed
                    ? this.reduceName(user.firstname + " " + user.name)
                    : user.firstname + " " + user.name;

                return (
                    <Layout style={{ minHeight: "100vh" }}>
                        <Sider
                            collapsible
                            collapsed={this.state.collapsed}
                            onCollapse={this.onCollapse}
                        >
                            <div
                                className="user-menu"
                                style={{ margin: "auto", width: "80%" }}
                            >
                                <div
                                    style={{
                                        display: "flex",
                                        margin: "auto",
                                        justifyContent: "center"
                                    }}
                                >
                                    <img
                                        src={backgroundImage}
                                        height="43px"
                                        width="auto"
                                        style={{
                                            marginTop: "1rem",
                                            marginBottom: "1rem"
                                        }}
                                        className="si-logo"
                                    />
                                </div>
                                <div className="user-profile">{name}</div>
                                <div className="user-permissions">
                                    {user.permissions}
                                </div>
                            </div>
                            <Menu theme="dark" mode="inline">
                                <Item key="1">
                                    <NavLink
                                        activeClassName="active"
                                        to="/home"
                                    >
                                        <Icon type="home" />
                                        <span>Accueil</span>
                                    </NavLink>
                                </Item>
                                <Item key="2">
                                    <NavLink
                                        activeClassName="active"
                                        to="/dimensionning"
                                    >
                                        <Icon type="select" />
                                        <span>Dimensionnement</span>
                                    </NavLink>
                                </Item>
                                <Item key="3">
                                    <NavLink
                                        activeClassName="active"
                                        to="/monitoring"
                                    >
                                        <Icon type="search" />
                                        <span>Surveillance</span>
                                    </NavLink>
                                </Item>
                                <SubMenu
                                    key="sub2"
                                    title={
                                        <span>
                                            <Icon type="setting" />
                                            <span>Paramètres</span>
                                        </span>
                                    }
                                >
                                    <Item key="4">
                                        <NavLink
                                            activeClassName="active"
                                            to="/settings/profile"
                                        >
                                            Profil
                                        </NavLink>
                                    </Item>
                                    {user.permissions != "responsible" ? (
                                    <Item key="5">
                                        <NavLink
                                            activeClassName="active"
                                            to="/settings/subscriptions"
                                        >
                                            Abonnements
                                        </NavLink>
                                    </Item>)
                                    : console.log(user.permissions)}
                                </SubMenu>
                                {user.permissions == "root" ||
                                user.permissions == "admin" ? (
                                    <SubMenu
                                        key="sub1"
                                        title={
                                            <span>
                                                <Icon type="user" />
                                                <span>Admin</span>
                                            </span>
                                        }
                                    >
                                        <Item key="6">
                                            <NavLink
                                                activeClassName="active"
                                                to="/admin/users"
                                            >
                                                Utilisateurs
                                            </NavLink>
                                        </Item>
                                        <Item key="7">
                                            <NavLink
                                                activeClassName="active"
                                                to="/admin/subscriptions"
                                            >
                                                Abonnements
                                            </NavLink>
                                        </Item>
                                    </SubMenu>
                                ) : (
                                    console.log(user.permissions)
                                )}
                            </Menu>
                        </Sider>
                        <Layout>
                            <Header style={{ background: "#fff", padding: 0 }}>
                                <div style={{ display : "flex", justifyContent : "space-between" }}>
                                    <div style={{ display : "flex", justifyContent : "space-between" }}>
                                        <a href="http://www.aedes.be/">
                                            <img
                                                src="./assets/aedes.jpg"
                                                style={{
                                                    height: "100%",
                                                    padding: "5"
                                                }}
                                            />
                                        </a>
                                        <a href="http://www.maisordi.com/">
                                            <img
                                                src="./assets/maisordi.png"
                                                style={{
                                                    height: "100%",
                                                    padding: "5"
                                                }}
                                            />
                                        </a>
                                    </div>
                                    <div style={{ display: "flex", justifyContent: "space-between", marginRight : "1rem" }}>
                                        <span onClick={this.info}>
                                            <Icon type="question-circle-o" style={{ fontSize : "45", padding : "0.5rem" }}/>
                                        </span>
                                        <Button style={{ margin: "auto" }} size="large" type="danger"><NavLink
                                                activeClassName="active"
                                                to="/logout"
                                            >
                                                Déconnexion
                                            </NavLink>
                                        </Button>
                                    </div>
                                </div>
                            </Header>
                            <Content style={{ margin: "2rem 2rem" }}>
                                {/* <div
                                    style={{
                                        padding: 24,
                                        minHeight: 360
                                    }}
                                > */}
                                {this.props.children}
                                {/* </div> */}
                            </Content>
                            <Footer style={{ textAlign: "center" }}>
                                Créé par Martin Delobbe
                            </Footer>
                        </Layout>
                    </Layout>
                );
            }
        }
    )
);
