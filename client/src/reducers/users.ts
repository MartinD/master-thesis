import { Actions } from "../actions/Actions"

const initialState = {
    users: [],
    status : ""
};

export default (state = initialState, action = { type: "", users: [], status: "" }) => {
    switch (action.type) {
        case Actions.SET_USERS:
            return {
                users : action.users,
                status : action.status
            };
        default:
            return state;
    }
};
