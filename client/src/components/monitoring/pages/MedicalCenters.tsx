import * as React from "react";
import { Card, Row, Col, Icon } from "antd";
import { connect } from "react-redux";
import { withRouter, Redirect, Link } from "react-router-dom";
import { fetchAllMonitoredCenters, fetchMonitoredCenters, fetchMonitoringAlerts } from "../../../actions/Monitoring";
import * as _ from "lodash"
import MapContainer from "./GoogleMapContainer"

export const MedicalCenters = withRouter(
    connect(
        state => {
            return {
                auth: state.auth,
                monitoring: state.monitoring,
                monitoringAlerts : state.monitoringAlerts
            };
        },
        { fetchAllMonitoredCenters, fetchMonitoredCenters, fetchMonitoringAlerts }
    )(
        class MedicalCenters extends React.Component<any, any> {
            
            componentWillMount() {
                if(this.props.auth.user.permissions == "technician" || this.props.auth.user.permissions == "responsible"){
                    this.props.fetchMonitoredCenters({
                        userKey : this.props.auth.user.id
                    }).then(() => {
                        this.props.fetchMonitoringAlerts({monitoring : this.props.monitoring.monitoring.map(element => {
                            return element.id
                        })})
                    });
                } else {
                    this.props.fetchAllMonitoredCenters().then(() => {
                        this.props.fetchMonitoringAlerts({monitoring : this.props.monitoring.monitoring.map(element => {
                            return element.id
                        })})
                    });
                }
            }

            render() {
                
                return (
                    <div>   
                        <div style={{ height : 450, position : "relative", width : "100%", marginBottom : "1rem" }}>
                            <MapContainer monitoring={this.props.monitoring.monitoring} monitoringAlerts={this.props.monitoringAlerts.data}/>
                        </div>
                        <Row gutter={16}>
                            {this.props.monitoring.monitoring.map(
                                (medicalCenter, key) => {
                                    return (
                                        <Col
                                            className="gutter-row"
                                            span={8}
                                            key={medicalCenter.id}
                                            style={{paddingBottom : 16}}
                                        >
                                            <MedicalCenterCard
                                                title={medicalCenter.name}
                                                warning={_.filter(this.props.monitoringAlerts.data, (alert) => { 
                                                    return alert.monitoringKey == medicalCenter.id 
                                                })}
                                                id={medicalCenter.id}
                                            />
                                        </Col>
                                    );
                                }
                            )}
                        </Row>
                    </div>
                );
            }
        }
    )
);

export interface MedicalCenterCardOwnProps {
    title: string;
    warning?: any;
    id: string;
}

class MedicalCenterCard extends React.Component<
    MedicalCenterCardOwnProps,
    any
> {
    render() {
        const className = this.props.warning.length > 0 ? "alert" : "noAlert";
        const warning = this.props.warning.length > 0 ? <div className="data-warning"><Icon type="close-circle" /> {this.props.warning.length} alerte(s) non résolue(s)</div> : <div className="data-warning"><Icon type="check-circle" /> Tout est en ordre !</div>
        return (
            <Link to={`/monitoring/centers-data/${this.props.id}`}>
                <Card hoverable title={this.props.title} className={className}>
                    <div className="warning">
                        {warning}
                    </div>
                </Card>
            </Link>
        );
    }
}
