import {create} from "../tests/db"


create("997", {
    id : "997",
    ip : "192.168.1.4",
    name : "Tablette 2",
    deviceType : "1",
    monitoringKey : "10",
    type : "device"
})

create("998", {
    id : "998",
    ip : "192.168.1.1",
    name : "Wifi",
    deviceType : "0",
    monitoringKey : "10",
    type : "device"
})

create("999", {
    id : "999",
    ip : "192.168.1.2",
    name : "Tablette 1",
    deviceType : "1",
    monitoringKey : "10",
    type : "device"
})