export const countryPrefix = [
    {
        name : "Afrique du Sud",
        prefix : "27"
    },
    {
        name : "Algérie",
        prefix :  "213"
    },
    {
        name : "Angola",
        prefix :  "244"
    },
    {
        name : "BéninNovo",
        prefix : "229"
    },
    {
        name : "Botswana",
        prefix : "267"
    },
    {
        name : "Burkina Faso",
        prefix : "226"
    },
    {
        name : "Burundi",
        prefix : "257"
    },
    {
        name : "Cameroun",
        prefix : "237"
    },
    {
        name : "Cap-Vert",
        prefix : "238"
    },
    {
        name : "Centrafrique",
        prefix : "236"
    },
    {
        name : "Congo",
        prefix : "242"
    },
    {
        name : "Congo RDC",
        prefix : "243"
    },
    {
        name : "Côte d'Ivoire",
        prefix : "225"
    },
    {
        name : "Le Djibouti",
        prefix : "253"
    },
    {
        name : "Egypte",
        prefix : "20"
    },
    {
        name : "Erythrée",
        prefix : "291"
    },
    {
        name : "EthiopieAbeba",
        prefix : "251"
    },
    {
        name : "Gabon",
        prefix : "241"
    },
    {
        name : "Gambie",
        prefix : "220"
    },
    {
        name : "Ghana",
        prefix : "233"
    },
    {
        name : "Guinée",
        prefix : "224"
    },
    {
        name : "Guinée équatoriale",
        prefix : "240"
    },
    {
        name : "Guinée-Bissau",
        prefix : "245"
    },
    {
        name : "Kenya",
        prefix : "254"
    },
    {
        name : "Lesotho",
        prefix : "266"
    },
    {
        name : "Libéria",
        prefix : "231"
    },
    {
        name : "Libye",
        prefix : "218"
    },
    {
        name : "Madagascar",
        prefix : "261"
    },
    {
        name : "Malawi",
        prefix : "265"
    },
    {
        name : "Mali",
        prefix : "223"
    },
    {
        name : "Maroc",
        prefix : "212"
    },
    {
        name : "Mauritanie",
        prefix : "222"
    },
    {
        name : "Mozambique",
        prefix : "258"
    },
    {
        name : "Namibie",
        prefix : "264"
    },
    {
        name : "Niger",
        prefix : "227"
    },
    {
        name : "Nigeria",
        prefix : "234"
    },
    {
        name : "Ouganda",
        prefix : "256"
    },
    {
        name : "Rwanda",
        prefix : "250"
    },
    {
        name : "Sao Tomé-et-PrincipeTomé",
        prefix : "239"
    },
    {
        name : "Sénégal",
        prefix : "221"
    },
    {
        name : "Sierra Leone",
        prefix : "232"
    },
    {
        name : "Somalie",
        prefix : "252"
    },
    {
        name : "Soudan",
        prefix : "249"
    },
    {
        name : "Sud-Soudanou Djouba",
        prefix : "211"
    },
    {
        name : "Swaziland",
        prefix : "268"
    },
    {
        name : "Tanzanie",
        prefix : "255"
    },
    {
        name : "Tchad",
        prefix : "235"
    },
    {
        name : "Togo",
        prefix : "228"
    },
    {
        name : "Tunisie",
        prefix : "216"
    },
    {
        name : "Zambie",
        prefix : "260"
    },
    {
        name : "Zimbabwe",
        prefix : "263"
    }
]