import * as React from "react"
import { connect } from "react-redux"
import { Redirect } from "react-router-dom"
import { Status } from "../actions/Status"

export default function Authenticate(options ?: any){
    return (ComponentClass) => {
        return connect( state => {
            return {
                auth : state.auth
            }
        })(class CustomClass extends React.Component<any,any>{
            render(){
                if(this.props.auth.isAuthenticated || this.props.auth.status == Status.LOADING){
                    return (
                        <ComponentClass {...this.props}></ComponentClass>
                    ) 
                }
                return <Redirect to="/login"/>
            }
        })
    }
}

