import * as React from "react";
import { Map, InfoWindow, Marker, GoogleApiWrapper } from "google-maps-react";
import * as _ from "lodash"

export interface MapContainerOwnProps {
    monitoring: any;
    monitoringAlerts: any;
}

export class MapContainer extends React.Component<MapContainerOwnProps, any> {

    constructor(props){
        super(props)
        this.state = {
            showingInfoWindow: false,
            activeMarker: {},
            selectedPlace: {},
          };
    }


    onMarkerClick = (props, marker, e) => {
        this.setState({
            selectedPlace: props,
            activeMarker: marker,
            showingInfoWindow: true
          });
    }

    onMapClicked = (props) => {
        if (this.state.showingInfoWindow) {
          this.setState({
            showingInfoWindow: false,
            activeMarker: null
          })
        }
      };

    render() {
        const google = (window as any).google;
        const {monitoring, monitoringAlerts} = this.props;
        return (
            <Map
                google={google}
                zoom={12}
                className="google-maps"
                initialCenter={{ lat: -4.3311104, lng: 15.2969216 }}
                onClick={this.onMapClicked}
            >
                {monitoring.map((center, key) => {
                    var url = _.includes(monitoringAlerts.map(elem => elem.monitoringKey), center["id"]) ? "/assets/google-maps-marker-red.png" : "/assets/google-maps-marker-green.png";
                    return <Marker
                        name={center.name}
                        position={{
                            lat: center.addressLat,
                            lng: center.addressLng
                        }}
                        icon={{
                            url: url,
                            anchor: new google.maps.Point(32, 32),
                            scaledSize: new google.maps.Size(32, 42)
                        }}
                        onClick={this.onMarkerClick}
                        key={key}
                    />
                })}
                <InfoWindow
                    marker={this.state.activeMarker}
                    visible={this.state.showingInfoWindow}>
                        <div>
                        {this.state.selectedPlace.name}
                        </div>
                </InfoWindow>
            </Map>
        );
    }
}

export default GoogleApiWrapper({
    apiKey: "AIzaSyBWxXSBk_CSL7QO_gamNB8ty6qTn9i1lUg"
})(MapContainer);
