import * as React from "react"
import {Route, Switch, Redirect} from "react-router-dom"
import {MedicalCenters} from "./pages/MedicalCenters"
import {MedicalCenterData} from "./pages/MedicalCenterData"

export class Routes extends React.Component<any, any> {

    render(){
        return (
            <Switch>
                <Redirect exact from="/monitoring" to="/monitoring/dashboard"/>
                <Route path="/monitoring/dashboard" component={MedicalCenters}/>
                <Route path="/monitoring/centers-data/:id" component={MedicalCenterData} />
            </Switch>
        )
    }

}