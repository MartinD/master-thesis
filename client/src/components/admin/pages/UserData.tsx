import * as React from "react"
import {connect, DispatchProp} from "react-redux"
import {UserForm} from "../../shared-components/UserForm"
import {updateUser, findUserByID} from "../../../actions/Users"
import {Card} from "antd"

export interface UserDataOwnProps{
    auth : any,
    users : any,
    updateUser : any
}

export const UserData = connect(
    state => {
        return {
            auth : state.auth,
            users: state.users
        };
    },
    { updateUser, findUserByID}
)(
    class UserData extends React.Component<DispatchProp<any>&UserDataOwnProps, any> {

        componentWillMount(){
            
            this.props.findUserByID(this.props.match.params["id"])
        }


        render() {
            return (
                <Card title="Données d'utilisateur" style={{ maxWidth : 800, margin : "auto" }}>
                <UserForm
                    actionProfile={this.props.updateUser}
                    user={this.props.users.users[0]}
                    auth={this.props.auth.user}
                />
                </Card>
            );
        }
    }
);

export default UserData;