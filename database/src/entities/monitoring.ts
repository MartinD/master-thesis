import { create } from "../tests/db";

create("10", {
    id: "10",
    name: "Test center",
    responsible: "Vianney",
    address: "31 rue de la damidaine Kinshasa, RDC",
    addressLat : -4.3311104,
    addressLng : 15.2969216,
    phone: "1234567",
    type: "monitoring",
    isOnline: "1"
});

// create("11", {
//     id: "11",
//     name: "CHMI",
//     responsible: "Donald",
//     address: "31, rue de la chasse royale Cotonou, Bénin",
//     addressLat : -4.351104,
//     addressLng : 15.3169216,
//     phone: "1234567",
//     type : "monitoring",
//     isOnline : "1"
// });
