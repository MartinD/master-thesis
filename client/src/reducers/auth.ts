import { Actions } from "../actions/Actions";
import * as _ from "lodash";

const initialState = {
    isAuthenticated: false,
    user: {}
};

export default (state = initialState, action = { type: "", user: {}, status : "" }) => {
    switch (action.type) {
        case Actions.SET_CURRENT_USER:
            return {
                isAuthenticated: !_.isEmpty(action.user),
                user: action.user,
                status: action.status
            };
        default:
            return state;
    }
};
