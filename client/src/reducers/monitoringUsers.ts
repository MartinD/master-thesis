import { Actions } from "../actions/Actions"

const initialState = {
    monitoringUsers : [],
    status : ""
}

export default (state = initialState, action = { type: "", monitoringUsers : [], status : "" }) => {
    switch (action.type) {
        case Actions.SET_MONITORING_USERS:
            return {
                monitoringUsers : action.monitoringUsers,
                status : action.status
            };
        default:
            return state;
    }
};