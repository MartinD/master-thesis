export const SmsFromTwilio = sms => {
    const smsFormated = {
        id: sms.SmsSid,
        body: sms.Body,             //Les sms envoyés doivent être sous format JSON
        from: sms.From,
        to: sms.To
    };
    return smsFormated;
};
