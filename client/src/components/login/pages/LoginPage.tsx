import * as React from "react";
import { Form, Icon, Input, Button, Card, Layout, message, Spin } from "antd";
import { FormComponentProps } from "antd/lib/form";
import { connect } from "react-redux";
import * as PropTypes from "prop-types";
import { userLoginRequest } from "../../../actions/Auth";

const FormItem = Form.Item;

export interface LoginFormOwnProps extends FormComponentProps {
    userLoginRequest: (values) => Promise<void>;
}

interface IRouter {
    history: any;
}

interface IRouterContext {
    router: IRouter;
}

class LoginForm extends React.Component<LoginFormOwnProps, any> {
    context: IRouterContext;
    static contextTypes = {
        router: PropTypes.object.isRequired
    };

    state = {
        email: "",
        password: "",
        errors: { form : ""},
        isLoading: false
    };

    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                console.log("Received values of form: ", values);
                this.setState({ errors: {}, isLoading: true });
                this.props
                    .userLoginRequest(values)
                    .then(res => {
                        message.success("Bienvenue");
                        this.context.router.history.push("/");
                    })
                    .catch(err => {
                        message.error("Identifiants invalides");
                        this.setState({
                            errors: err.response.data.errors,
                            isLoading: false
                        });
                    });
            }
        });
    };

    onChange = e => {
        this.setState({
            [e.target.id] : e.target.value
        })
    }

    render() {
        const { errors, email, password, isLoading } = this.state
        const { getFieldDecorator } = this.props.form;
        return (
            <Card>
                <Spin
                    spinning={isLoading}
                    className="spin-login-form"
                >
                    <Form onSubmit={this.handleSubmit} className="login-form">
                        <FormItem>
                            {getFieldDecorator("email", {
                                rules: [
                                    { 
                                        type : "email",
                                        message : "Veuillez insérer une adresse valide!"
                                    },
                                    {
                                        required: true,
                                        message: "Veuillez insérer votre email!"
                                    }
                                ]
                            })(
                                <Input
                                    prefix={
                                        <Icon
                                            type="user"
                                            style={{ color: "rgba(0,0,0,.25)" }}
                                        />
                                    }
                                    placeholder="Email"
                                    onChange={this.onChange}
                                />
                            )}
                        </FormItem>
                        <FormItem>
                            {getFieldDecorator("password", {
                                rules: [
                                    {
                                        required: true,
                                        message:
                                            "Veuillez insérer votre mot de passe!"
                                    }
                                ]
                            })(
                                <Input
                                    prefix={
                                        <Icon
                                            type="lock"
                                            style={{ color: "rgba(0,0,0,.25)" }}
                                        />
                                    }
                                    type="password"
                                    placeholder="Mot de passe"
                                    onChange={this.onChange}
                                />
                            )}
                        </FormItem>
                        <FormItem>
                            <Button
                                type="primary"
                                htmlType="submit"
                                className="login-form-button"
                                style={{ width: "100%", marginBottom : "-1rem" }}
                            >
                                S'identifier
                            </Button>
                        </FormItem>
                    </Form>
                </Spin>
            </Card>
        );
    }
}

const WrappedNormalLoginForm = Form.create()(LoginForm);

export class LoginPage extends React.Component<any, any> {
    render() {
        const { userLoginRequest } = this.props;
        return (
            <Layout className="login-page">
                <Layout.Content>
                    <WrappedNormalLoginForm
                        userLoginRequest={userLoginRequest}
                    />
                </Layout.Content>
            </Layout>
        );
    }
}

export default connect(
    state => {
        return {};
    },
    { userLoginRequest }
)(LoginPage);
