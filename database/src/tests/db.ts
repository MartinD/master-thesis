require("dotenv").config();
const couchbase = require("couchbase");
const curl = require("curlrequest");
const cluster = new couchbase.Cluster("http://cerhisprod.org");
cluster.authenticate(
    process.env.COUCHBASE_USERNAME,
    process.env.COUCHBASE_PASSWORD
);

var N1qlQuery = couchbase.N1qlQuery;
let bucket = cluster.openBucket(process.env.COUCHBASE_BUCKET);

const defaultCallbackCreate = (err, res) => {
    if (err) console.log("Erreur lors de la création");
    else console.log("Entité créée");
};

const create = (name, data, callback = defaultCallbackCreate) => {
    bucket.upsert(name, data, callback);
};

const query = (query: any, callback) => {
    bucket.httpsQuery(N1qlQuery.fromString(query), callback);
};

const httpsQuery = (query , callback) => {

    curl.request({
        url : "https://cerhisprod.org:18093/query/service",
        data : `statement=${query}&creds=[{"user":"${process.env.COUCHBASE_USERNAME}", "pass":"${process.env.COUCHBASE_PASSWORD}"}]"`,
        insecure: true,
    }, (err, data) => {
        if(!err) callback(JSON.parse(data).errors, JSON.parse(data).results, { status : JSON.parse(data).status, metrics : JSON.parse(data).metrics});
        else console.log("Requête HTTP non valide")
    })

}

export { create, query, httpsQuery };
