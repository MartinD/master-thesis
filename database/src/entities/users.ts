import { create, httpsQuery } from "../tests/db";
import * as bcrypt from "bcrypt";


create("0", {
    name: "Delobbe",
    firstname: "Martin",
    email: "mrt.dlb@gmail.com",
    pswd: bcrypt.hashSync("bonjour", 10),
    permissions: "root",
    phone: "970652708",
    country: "Congo RDC",
    prefix: "243",
    id: "0",
    type: "user"
});

// create("1", {
//     name: "Berlanger",
//     firstname: "Hadrien",
//     email: "hadrien@gmail.com",
//     pswd: bcrypt.hashSync("password", 10),
//     permissions: "admin",
//     phone: "+243970652708",
//     country: "Congo",
//     prefix: "242",
//     id: "1",
//     type: "user"
// });

// create("2", {
//     name: "Berlanger",
//     firstname: "Martin",
//     email: "martin@gmail.com",
//     pswd: bcrypt.hashSync("password", 10),
//     permissions: "technician",
//     phone: "+243970652708",
//     country: "Congo",
//     prefix: "242",
//     id: "2",
//     type: "user"
// });

// create("3", {
//     name: "Perrine",
//     firstname : "Franco",
//     email : "perrine@mail.com",
//     pswd: bcrypt.hashSync("password", 10),
//     permissions: "technician",
//     phone : "0970652708",
//     country: "Congo RDC",
//     prefix : "243",
//     id : "3",
//     type : "user"
// })
