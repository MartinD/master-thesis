"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Actions = {
    CREATE: "create",
    UPDATE: "update",
    DELETE: "delete"
};
