require('dotenv').config()
const express = require("express")
const path = require("path")
const bodyParser = require("body-parser")

import users from "./routers/users"
import auth from "./routers/auth"
import monitoring from "./routers/monitoring"
import sms from "./routers/sms"
import {sendSmsTwilio} from "./models/sms"

var couchbase = require("couchbase")
var cluster = new couchbase.Cluster("couchbase://127.0.0.1")
cluster.authenticate(process.env.COUCHBASE_USERNAME, process.env.COUCHBASE_PASSWORD);
var bucket = cluster.openBucket("CERHIS_MONITOR")

let app = express();

app.use(bodyParser.json()) //data available in request.body

app.use("/api/users", users)
app.use("/api/auth", auth)
app.use("/api/monitoring", monitoring)
app.use("/api/sms", sms)
app.use(express.static(path.resolve(__dirname, '../../client/build')));
app.get('*', function (req, res) {
    res.sendFile(path.resolve(__dirname, '../../client/build/index.html'));
    res.end();
  });

app.listen(8081, () => console.log("Running on localhost:8081"))