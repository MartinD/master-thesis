import * as React from "react";
import { Card } from "antd";
import { connect } from "react-redux";

export const Home = connect(
    state => {
        return {
            auth : state.auth
        }
    }
)(
    class Home extends React.Component<any, any> {
    render() {
        let rights;
        switch(this.props.auth.user.permissions){
            case "technian":
                rights = (
                    <ul>
                        <li>Consulter les données des différents centres</li>
                        <li>Modifier vos données de profil </li>
                        <li>Décider de recevoir ou non des SMS d'alerte</li>
                    </ul>
                )
                break;
            case "responsible":
                rights = (
                    <ul>
                        <li>Consulter les données des différents centres</li>
                        <li>Modifier vos données de profil </li>
                    </ul>
                )
                break;
            case "admin":
                rights = (
                    <ul>
                    <li>Consulter les données des différents centres</li>
                    <li>Modifier vos données de profil </li>
                    <li>Décider de recevoir ou non des SMS d'alerte</li>
                    <li>Décider de recevoir ou non des SMS journaliers</li>
                    <li>
                        Créer, mettre à jour ou même supprimer un
                        utilisateur
                    </li>
                    <li>
                        Gérer les abonnements des différents utilisateurs
                    </li>
                </ul>
                )
                break;
            case "root":
                rights = (
                    <ul>
                    <li>Consulter les données des différents centres</li>
                    <li>Modifier vos données de profil </li>
                    <li>Décider de recevoir ou non des SMS d'alerte</li>
                    <li>Décider de recevoir ou non des SMS journaliers</li>
                    <li>
                        Créer, mettre à jour ou même supprimer un
                        utilisateur
                    </li>
                    <li>
                        Gérer les abonnements des différents utilisateurs
                    </li>
                    <li>Il est impossible de vous supprimer </li>
                    </ul>
                )
                break;
        }
        const technicianRights = (
            <div>
                <h3>Décider ou non de recevoir des SMS d'alerte</h3>
                <p>En vous rendant dans le sous-menu "Abonnements" de l'onglet "Paramètres du
                    menu de gauche, vous pouvez consulter tous les centres auxquels vous êtes
                    abonné. Être abonné à un centre signifie que vous recevrez quotidiennement
                    un SMS de rapport sur l'état du système. En cliquant sur un des centres, 
                    vous voyez apparaître la liste des services monitorés dans le centre. Tous les
                    services cochés correspondent aux services pour lesquels vous recevrez une
                    notification par SMS en cas d'alerte ou de retour à la normale. Décocher un
                    service signifie que vous ne recevrez plus de SMS mais cela ne vous empêchera
                    pas de consulter les données du centre.
                </p>
            </div>
        )
        const adminRights = (
            <div>
                <h3>Décider ou non de recevoir des SMS journaliers</h3>
                <p>
                    Cette fonctionnalité propre aux admin permet de gérer les abonnements des autres
                    utilisateurs ainsi que les siens. Afin de profiter de cette feature, se rendre 
                    dans l'onglet "Admin" et sélectionner "Abonnements". Sélectionner l'onglet "En
                    fontion des utilisateurs", cliquer sur votre nom d'utilisateur et cocher/décocher
                    les centres auxquels vous abonner ou non.
                </p>
                <h3>Créer, mettre à jour ou même supprimer un utilisateur</h3>
                <p>
                    Cette gestion des utilisateurs vous permet de donner des rôles spécifiques
                    à tout nouvel utilisateur ou même de modifier ses droits lorsqu'un changement
                    social ou professionel apparaît.
                </p>
                <h3>Gérer les abonnements des différents utilisateurs</h3>
                <p>
                    De même que pour la gestion de vos propres abonnements, vous pouvez modifier ceux
                    des autres utilisateurs. Cela se réalise de la même manière et vous pouvez soit
                    partir des centres de santé vers les utilisateurs abonnés vers les services soit 
                    depuis les utilisateurs vers les centres de santé vers les services pour cocher 
                    les différents abonnements que vous voulez attribuer à l'utilisateur. Rappelez-vous
                    qu'un "technicien" ne peut consulter les données que des centres auxquels il est abonné,
                    qu'abonner un utilisateur à un centre signifie qu'il recevra les SMS journaliers de rapport
                    et que pour recevoir les SMS d'alerte, il faut en plus cocher les différents services
                    présents dans le centre en question.
                </p>
            </div>
        ) 
        const rootRights = (
            <div>
                <h3>Il est impossible de vous supprimer</h3>
                <p>You're the boss</p>
            </div>
        )
        return (
            <div className="home-content">
                <Card>
                    <h1>Cerhis Monitor</h1>
                    <h2>Introduction</h2>
                    <p>
                        Bienvenue sur Cerhis Monitor! Cette plateforme vous
                        permet de consulter les informations en temps réel des
                        centres hospitaliers inclus dans la base de données. Ces
                        informations sont soit relatives au dimensionnement des
                        centres hospitaliers soit à la surveillance de ces
                        derniers. Cette plateforme, en plus de fournir une
                        représentation visuelles des données emmagasinées, sert
                        également de relais pour les alertes générées par les
                        systèmes de monitoring dans les centres de santé. De la
                        sorte, les personnes abonnées à certains centres et aux
                        services de ces centres, recevront les SMS les prévenant
                        qu'un problème est survenu ou que ce dernier a été
                        résolu.
                    </p>
                    <h2>Utilisation de l'application</h2>
                    <p>
                        L'utilisation que vous pouvez faire de cette application
                        dépend en grande partie des droits qui vous sont
                        attribués. Ces droits dépendent de votre statut dans
                        l'application et ce statut est directement visible en
                        haut du menu de gauche. Dans votre cas vous êtes{" "}
                        <b>{this.props.auth.user.permissions}</b>. Cela signifie que vous avez les droits
                        suivants. Vous pouvez :
                    </p>
                    {rights}
                    <p>
                        Toutes ces fonctionnalités sont certes attirantes mais
                        encore faut-il savoir les utiliser! C'est la raison pour
                        laquelle nous allons voir pas à pas comment en profiter
                        au mieux et ce, de la plus simple des manières.
                    </p>
                        <h3>Consulter les données des différents centres</h3>
                        <p>Pour ce faire, rendez-vous dans l'onglet Surveillance. Tous les centres monitorés 
                            sont répertoriés dans cet onglet. Lorsqu'un problème n'a pas encore été résolu, 
                            le centre sera représenté en rouge. Si vous cliquez sur le centre, les données 
                            des différents services monitorés sont directement accessibles. Si un problème 
                            n'est pas encore résolu, une icône "attention" sera visible à côté du nom donné 
                            au service.
                        </p>

                        <h3>Modifier vos données de profil</h3>
                        <p>En vous rendant dans le sous-menu "Profil" de l'onglet "Paramètres" du menu de 
                            gauche, vous pouvez consulter toutes vos données personnelles relatives à 
                            votre profil. Pour les modifier, il vous suffit de changer les données et de 
                            cliquer ensuite sur "Enregistrer". Pour définir un nouveau mot de passe, cliquer
                            sur "Nouveau mot de passe" et compléter les champs relatifs.
                        </p>

                        {this.props.auth.user.permissions == "technician" || this.props.auth.user.permissions == "admin" || this.props.auth.user.permissions == "root" ? technicianRights : null}
                        {this.props.auth.user.permissions == "admin" || this.props.auth.user.permissions == "root" ? adminRights : null}
                        {this.props.auth.user.permissions == "root" ? rootRights : null}
                        
                </Card>
            </div>
        );
    }
})
