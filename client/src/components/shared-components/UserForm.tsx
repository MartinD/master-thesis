import * as React from "react";
import * as _ from "lodash";
import * as uuid from "uuid";
import { Form, Button, Input, Select, message, Spin } from "antd";
import { FormComponentProps } from "antd/lib/form";
import { countryPrefix } from "../../resources/CountryPrefix";

const FormItem = Form.Item;
const Option = Select.Option;

export interface UserFormOwnProps extends FormComponentProps {
    actionProfile: (values) => Promise<any>;
    user: any;
    auth: any;
}

export const UserForm = Form.create()(
    class UserForm extends React.Component<UserFormOwnProps, any> {
        state = {
            isLoading: false,
            email: "",
            password: "",
            errors: {
                email: ""
            },
            changePswd: false,
            confirmDirty: false
        };

        componentDidMount() {
            // this.handleCountryChange(this.props.user.country)
        }

        handleSubmit = e => {
            // très important d'en faire un thunk autrement le props form n'est pas reconnu (voir applymiddleware dans index.tsx)
            e.preventDefault();
            this.props.form.validateFields((err, values) => {
                if (!err) {
                    this.setState({
                        isLoading: true,
                        errors: {}
                    });
                    values.id = this.props.user
                        ? this.props.user.id
                        : uuid.v4(); // important pour identifier l'utilisateur à changer
                    values.type = "user";
                    //console.log("Received values of form: ", values);
                    this.props
                        .actionProfile(values)
                        .then(res => {
                            this.setState({
                                isLoading: false,
                                errors: {}
                            });
                            message.success("Données enregistrées");
                        })
                        .catch(err => {
                            this.setState({
                                isLoading: false,
                                errors: err.response.data.errors
                            });
                            message.error("Erreur lors de l'enregistrement");
                        });
                }
            });
        };

        handlePrefixChange = prefix => {
            var country = this.prefixToCountry(prefix);
            this.props.form.setFieldsValue({
                country: `${country}`
            });
        };

        handleCountryChange = name => {
            var prefix = this.countryToPrefix(name);
            this.props.form.setFieldsValue({
                prefix: `${prefix}`
            });
        };

        prefixToCountry(prefix) {
            for (var i = 0; i < countryPrefix.length; i++) {
                if (countryPrefix[i].prefix == prefix) {
                    return countryPrefix[i].name;
                }
            }
        }

        countryToPrefix(name) {
            for (var i = 0; i < countryPrefix.length; i++) {
                if (countryPrefix[i].name == name) {
                    return countryPrefix[i].prefix;
                }
            }
        }

        changePswd = () => {
            this.setState({
                changePswd: !this.state.changePswd
            });
        };

        checkPassword = (rule, value, callback) => {
            const form = this.props.form;
            if (value && value !== form.getFieldValue("pswd")) {
                callback("Les deux mots de passe ne sont pas identiques!");
            } else {
                callback();
            }
        };

        checkConfirm = (rule, value, callback) => {
            const form = this.props.form;
            if (value && this.state.confirmDirty) {
                form.validateFields(["confirm"], { force: true }, () => {});
            }
            callback();
        };

        handleConfirmBlur = e => {
            const value = e.target.value;
            this.setState({ confirmDirty: this.state.confirmDirty || !!value });
        };

        render() {
            const { getFieldDecorator } = this.props.form;
            const { errors, password, email, isLoading } = this.state;

            const formItemLayout = {
                labelCol: {
                    xs: { span: 16 },
                    sm: { span: 7 }
                },
                wrapperCol: {
                    xs: { span: 16 },
                    sm: { span: 12 }
                }
            };
            
            const user = this.props.user || {};

            return (
                <Spin spinning={this.state.isLoading || this.props.user == undefined}>
                    <Form onSubmit={this.handleSubmit} className="user-form">
                        <FormItem {...formItemLayout} label="Email">
                            {getFieldDecorator("email", {
                                rules: [
                                    {
                                        type: "email",
                                        message:
                                            "Veuillez insérer une adresse valide!"
                                    },
                                    {
                                        required: true,
                                        message: "Veuillez insérer votre email!"
                                    }
                                ],
                                initialValue: user.email
                            })(<Input placeholder="Email" />)}
                            {/* {errors.email && (
                                <div className="alert alert-danger">
                                    {errors.email}
                                </div>
                            )} */}
                        </FormItem>
                        <FormItem {...formItemLayout} label="Nom">
                            {getFieldDecorator("name", {
                                rules: [
                                    {
                                        required: false
                                    }
                                ],
                                initialValue: user.name
                            })(<Input placeholder="Nom" />)}
                        </FormItem>
                        <FormItem {...formItemLayout} label="Prénom">
                            {getFieldDecorator("firstname", {
                                rules: [
                                    {
                                        required: false
                                    }
                                ],
                                initialValue: user.firstname
                            })(<Input placeholder="prénom" />)}
                        </FormItem>
                        <FormItem {...formItemLayout} label="Pays">
                            {getFieldDecorator("country", {
                                rules: [
                                    {
                                        required: true,
                                        message:
                                            "Veuillez sélectionner un pays!"
                                    }
                                ],
                                initialValue: user.country
                            })(
                                <Select
                                    placeholder="Sélectionnez un pays"
                                    onChange={this.handleCountryChange}
                                >
                                    {countryPrefix.map((country, key) => {
                                        return (
                                            <Option
                                                value={`${country.name}`}
                                                key={key}
                                            >
                                                {country.name}
                                            </Option>
                                        );
                                    })}
                                </Select>
                            )}
                        </FormItem>
                        <FormItem
                            {...formItemLayout}
                            label="Numéro de portable"
                        >
                            {getFieldDecorator("phone", {
                                rules: [
                                    {
                                        required: true,
                                        message:
                                            "Veuillez insérer votre numéro de portable!"
                                    }
                                ],
                                initialValue: user.phone
                            })(
                                <Input
                                    addonBefore={getFieldDecorator("prefix", {
                                        initialValue: user.prefix
                                    })(
                                        <Select
                                            style={{ width: 80 }}
                                            onChange={this.handlePrefixChange}
                                        >
                                            {countryPrefix.map(
                                                (country, key) => {
                                                    return (
                                                        <Option
                                                            value={
                                                                country.prefix
                                                            }
                                                            key={key}
                                                        >
                                                            +{country.prefix}
                                                        </Option>
                                                    );
                                                }
                                            )}
                                        </Select>
                                    )}
                                />
                            )}
                        </FormItem>
                        <FormItem {...formItemLayout} label="permissions">
                            {getFieldDecorator("permissions", {
                                rules: [
                                    {
                                        required: true,
                                        message:
                                            "Veuillez sélectionner une permission!"
                                    }
                                ],
                                initialValue: user.permissions
                            })(
                                <Select
                                    placeholder="Sélectionnez une permission"
                                    disabled={
                                        this.props.auth.permissions ==
                                        "technician" || this.props.auth.permissions == "responsible"
                                    }
                                >
                                    <Option value="root">Root</Option>
                                    <Option value="admin">Admin</Option>
                                    <Option value="responsible">Responsable</Option>
                                    <Option value="technician">
                                        Technicien
                                    </Option>
                                </Select>
                            )}
                        </FormItem>
                        {this.state.changePswd ? (
                            <FormItem
                                {...formItemLayout}
                                label="Nouveau mot de passe"
                            >
                                {getFieldDecorator("pswd", {
                                    rules: [
                                        {
                                            required: true,
                                            message:
                                                "Veuillez écrire un mot de passe!"
                                        },
                                        {
                                            validator: this.checkConfirm
                                        }
                                    ]
                                })(
                                    <Input
                                        placeholder="Mot de passe"
                                        type="password"
                                    />
                                )}
                            </FormItem>
                        ) : null}
                        {this.state.changePswd ? (
                            <FormItem
                                {...formItemLayout}
                                label="Confirmez le mot de passe"
                            >
                                {getFieldDecorator("confirm", {
                                    rules: [
                                        {
                                            required: true,
                                            message:
                                                "Confirmez votre mot de passe!"
                                        },
                                        {
                                            validator: this.checkPassword
                                        }
                                    ]
                                })(
                                    <Input
                                        type="password"
                                        onBlur={this.handleConfirmBlur}
                                    />
                                )}
                            </FormItem>
                        ) : null}
                        <FormItem {...formItemLayout} label="Mot de passe">
                            <Button onClick={this.changePswd}>
                                {!this.state.changePswd
                                    ? "Nouveau mot de passe"
                                    : "Annuler"}
                            </Button>
                        </FormItem>
                        <FormItem wrapperCol={{ span: 12, offset: 7 }}>
                            <Button
                                type="primary"
                                htmlType="submit"
                                className="login-form-button"
                                style={{ width: "100%" }}
                            >
                                Enregistrer
                            </Button>
                        </FormItem>
                    </Form>
                </Spin>
            );
        }
    }
);
