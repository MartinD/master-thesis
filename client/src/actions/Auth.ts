import axios from "axios";
import setAuthorizationToken from "../utils/setAuthorizationToken";
import * as jwt from "jsonwebtoken";
import { Actions } from "./Actions";
import { Status } from "./Status";

export function userLoginRequest(userData) {
  return dispatch => {
    dispatch({
      type: Actions.SET_CURRENT_USER,
      user: {},
      status: Status.LOADING
    });
    return axios
      .post("/api/auth", userData)
      .then(res => {
        const token = res.data.token;
        localStorage.setItem("jwtToken", token);
        setAuthorizationToken(token);
        dispatch({
          type: Actions.SET_CURRENT_USER,
          user: jwt.decode(token),
          status: Status.FETCHED
        });
      })
      .catch(err => {
        throw err;
      });
  };
}

export function userLogoutRequest() {
  return dispatch => {
    localStorage.removeItem("jwtToken");
    setAuthorizationToken(false);
    dispatch({
      type: Actions.SET_CURRENT_USER,
      user: {},
      status: Status.FETCHED
    }); // isAuthenticated to false and user empty object
  };
}
