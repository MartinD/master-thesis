import { Actions } from "../actions/Actions"

const initialState = {
    monitoringDeviceConfigs : []
}

export default (state = initialState, action = { type: "", monitoringDeviceConfigs : [], status : "" }) => {
    switch (action.type) {
        case Actions.SET_MONITORING_DEVICE_CONFIGS:
            return {
                monitoringDeviceConfigs : action.monitoringDeviceConfigs,
                status : action.status
            };
        default:
            return state;
    }
};