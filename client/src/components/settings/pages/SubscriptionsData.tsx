import * as React from "react";
import { connect } from "react-redux";
import {
    findMonitoredCenterById,
    fetchAllDevicesByCenter,
    fetchDeviceUsersByUser,
    updateDeviceSubscriptions
} from "../../../actions/Monitoring";
import { SubscriptionsForm } from "../../shared-components/SubscriptionsForm";

export const SubscriptionsData = connect(
    state => {
        return {
            auth: state.auth,
            monitoringDevices: state.monitoringDevices,
            monitoringDeviceUsers : state.monitoringDeviceUsers
        };
    },
    { fetchAllDevicesByCenter, findMonitoredCenterById, fetchDeviceUsersByUser, updateDeviceSubscriptions }
)(
    class SubscriptionsData extends React.Component<any, any> {
        state = {
            monitoringDevices: {},
            monitoringDeviceUsers: {},
            loading: true
        };

        componentWillMount() {
            this.props.fetchAllDevicesByCenter({
                monitoringKey: this.props.match.params["id"]
            }).then(() => {
                this.setState({
                    monitoringDevices: this.props.monitoringDevices.data
                });
            });
            this.props.fetchDeviceUsersByUser({
                userKey: this.props.auth.user.id,
                monitoringKey: this.props.match.params["id"]
            }).then(() => {
                this.setState({
                    monitoringDeviceUsers: this.props.monitoringDeviceUsers.data,
                    loading: false
                });
            });
        }

        render() {
            return (
                <div>
                    {!this.state.loading ? (
                        <SubscriptionsForm
                            actionSubscribe={this.props.updateSubscriptions}
                            userKey={this.props.auth.user.id}
                            monitoringKey={this.props.match.params["id"]}
                            monitoringDevices={
                                this.state.monitoringDevices
                            }
                            monitoringDeviceUsers={
                                this.state.monitoringDeviceUsers
                            }
                        />
                    ) : (
                        <h1>Données en cours de chargement...</h1>
                    )}
                </div>
            );
        }
    }
);
