import * as React from "react";
import * as _ from "lodash";
import { Table, Icon, Divider, Button, Spin, Modal, Card } from "antd";
import { withRouter, Redirect, Link } from "react-router-dom";
import { connect, DispatchProp } from "react-redux";
import { fetchUsers, deleteUser } from "../../../actions/Users";

const confirm = Modal.confirm;
const info = Modal.info;

export interface UserManagementOwnProps {
    auth: any;
    users: any;
}

export const UserManagement = withRouter(
    connect(
        state => {
            return {
                auth: state.auth,
                users: state.users
            };
        },
        { deleteUser, fetchUsers }
    )(
        class UserManagement extends React.Component<
            UserManagementOwnProps & DispatchProp<any>,
            any
        > {
            componentWillMount() {
                this.props.fetchUsers(this.props.auth.user);
            }

            confirmDelete = (id, permissions) => {
                if(permissions != "root"){

                    confirm({
                        title:
                        "Supprimer cet utilisateur ?",
                        content: "Vous ne pourrez pas faire marche arrière.",
                        okText: "Confirmer",
                        okType: "danger",
                        cancelText: "Annuler",
                        onOk: () => {
                            // doit etre un thunk !!
                            this.props.deleteUser(id).then(res => {
                                this.props.fetchUsers(this.props.auth.user);
                            });
                        }
                    });
                } else {
                    info({
                        title: "Suppression impossible",
                        content: "Vous ne pouvez pas supprimer ce type d'utilisateur",
                        onOk(){}
                    })
                }
            };

            render() {
                const columns = [
                    {
                        title: "Nom",
                        dataIndex: "name",
                        key: "name",
                        render: (text, record, index) => (
                            <Link to={`/admin/users-data/${record.key}`}>
                                {text}
                            </Link>
                        )
                    },
                    {
                        title: "Email",
                        dataIndex: "email",
                        key: "email"
                    },
                    {
                        title: "permissions",
                        dataIndex: "permissions",
                        key: "permissions"
                    },
                    {
                        title: "",
                        dataIndex: "",
                        key: "delete",
                        render: (text, record, index) => (
                            <Icon
                                type="delete"
                                onClick={() => {
                                    this.confirmDelete(record.key, record.permissions)
                                }
                            }
                            />
                        )
                    }
                ];

                const data = [];
                const users = this.props.users.users;
                for (var i = 0; i < users.length; i++) {
                    data.push({
                        key: users[i].id,
                        name: users[i].firstname + " " + users[i].name,
                        email: users[i].email,
                        permissions: users[i].permissions
                    });
                }

                const Title = () => {
                    return (
                        <span className="user-table-title">
                            Nombre d'utilisateurs ({this.props.users.users.length || 0})
                        </span>
                    );
                };
                return (
                    <Card title="Liste des utilisateurs">
                        <Spin spinning={this.props.users.status === "LOADING"}>
                            <Table
                                title={Title}
                                columns={columns}
                                dataSource={data}
                            />
                        </Spin>
                        <Link to="/admin/users-add">                
                            <Button
                                style={{ width: "100%"}}
                                type="primary"
                                >
                                Ajouter un utilisateur
                            </Button>
                        </Link>
                    </Card>
                );
            }
        }
    )
);
