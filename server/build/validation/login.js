"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var validator = require("validator");
var _ = require("lodash");
function validateInput(data) {
    var errors = {};
    if (validator.isEmpty(data.email)) {
        errors.email = "Ce champs est requis";
    }
    if (validator.isEmpty(data.password)) {
        errors.password = "Ce champs est requis";
    }
    return {
        errors,
        isValid: _.isEmpty(errors)
    };
}
exports.default = validateInput;
