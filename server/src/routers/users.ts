require("dotenv").config();
var express = require("express");
var jwt = require("jsonwebtoken");
var _ = require("lodash");
import * as bcrypt from "bcrypt";
import verifyRegister from "../validation/register";
import authenticate from "../middlewares/authenticate";
import permissions from "../middlewares/permissions";
import { httpsQuery, Create, Delete } from "../models/db";

let router = express.Router();

router.post("/profile/update", authenticate, (req, res) => {
    const user = req.body;
    const currentUser = req.currentUser;

    verifyRegister(user, (errors, isValid) => {
        if (!isValid) {
            res.status(400).json({ errors });
        } else {
            const query = _.isEmpty(user.pswd)
                ? `update CERHIS_MONITOR 
            set name='${user.name}', firstname='${user.firstname}', email='${
                      user.email
                  }', country='${user.country}', phone='${
                      user.phone
                  }', permissions='${user.permissions}' 
            where CERHIS_MONITOR.id='${currentUser.id}'`
                : `update CERHIS_MONITOR 
            set name='${user.name}', firstname='${user.firstname}', email='${
                      user.email
                  }', 
            pswd='${bcrypt.hashSync(user.pswd, 10)}', country='${
                      user.country
                  }', phone='${user.phone}', permissions='${user.permissions}' 
            where CERHIS_MONITOR.id='${currentUser.id}'`;

            httpsQuery(query, err => {
                if (!err) {
                    const token = jwt.sign(
                        {
                            name: user.name,
                            firstname: user.firstname,
                            email: user.email,
                            country: user.country,
                            phone: user.phone,
                            prefix: user.prefix,
                            permissions: user.permissions,
                            type: currentUser.type, //prevent from beeing able to change it
                            id: currentUser.id
                        },
                        process.env.JWT_SECRET
                    );
                    res.json({ token });
                } else {
                    res.status(401).json({ error: "Requete invalide" });
                }
            });
        }
    });
});

//ADMIN ROUTES --> permissions checked

router.post("/fetchAll", authenticate, permissions, (req, res) => {
    const query = `select CERHIS_MONITOR.* from CERHIS_MONITOR where CERHIS_MONITOR.type='user'`

    httpsQuery(query, (err, users, meta) => {
        for (var i = 0; i < users.length; i++) delete users[i].pswd;
        res.json({ users });
    });
});

router.get("/findUserByID/:id", authenticate, (req, res) => {
    const query = `select CERHIS_MONITOR.* from CERHIS_MONITOR where CERHIS_MONITOR.id='${
            req.params.id
        }' and CERHIS_MONITOR.type='user'`

    httpsQuery(query, (err, user, meta) => {
        if (!err) {
            delete user[0].pswd;
            res.json({ user });
        }
    });
});

router.post("/user/update", authenticate, permissions, (req, res) => {
    const user = req.body;
    verifyRegister(user, (errors, isValid) => {
        if (!isValid) {
            res.status(400).json({ errors });
        } else {
            const query = _.isEmpty(user.pswd)
                ? `update CERHIS_MONITOR
                set name='${user.name}', firstname='${
                          user.firstname
                      }', email='${user.email}', country='${
                          user.country
                      }', phone='${user.phone}', permissions='${
                          user.permissions
                      }' 
                where CERHIS_MONITOR.id='${user.id}'`
                : `update CERHIS_MONITOR
                set name='${user.name}', pswd='${bcrypt.hashSync(
                      user.pswd,
                      10
                  )}', firstname='${user.firstname}', email='${
                      user.email
                  }', country='${user.country}', phone='${
                      user.phone
                  }', permissions='${user.permissions}' 
                where CERHIS_MONITOR.id='${user.id}'`

            httpsQuery(query, err => {
                if (!err) {
                    delete user.pswd;
                    res.json({ users: [user] });
                } else {
                    res
                        .status(400)
                        .json({ errors: { user: "Utilisateur introuvable" } });
                }
            });
        }
    });
});

router.post("/add", authenticate, permissions, (req, res) => {
    var user = req.body;
    user.pswd = bcrypt.hashSync(user.pswd, 10); //hash password
    delete user.confirm;
    verifyRegister(user, (errors, isValid) => {
        if (!isValid) {
            res.status(400).json({ errors });
        } else {
            Create(user.id, user, (err, result) => {
                if (!err) {
                    res.json({ status: "valid" });
                } else {
                    res.status(401).json({
                        errors: {
                            user: "Impossible d'enregistrer l'utilisateur"
                        }
                    });
                }
            });
        }
    });
});

router.post("/user/delete/:id", authenticate, permissions, (req, res) => {
    const query = `delete from CERHIS_MONITOR c1 where c1.userKey="${req.params.id}" or c1.id="${req.params.id}"`
    httpsQuery(query, (err, result, meta) => {
        if(!err){
            res.json({ status : "valid" });
        } else {
            res.status(401).json({
                errors : "Impossible de supprimer l'utilisateur"
            })
        }
    })
});

export default router;
