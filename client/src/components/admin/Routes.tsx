import * as React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import { UserManagement } from "./pages/Users";
import { UserData } from "./pages/UserData";
import { UserAdd } from "./pages/UserAdd";
import { Subscriptions } from "./pages/Subscriptions";

export class Routes extends React.Component<any, any> {
    render() {
        return (
            <Switch>
                <Redirect exact from="/admin" to="/admin/users" />
                <Route path="/admin/users" component={UserManagement} />
                <Route path="/admin/users-add" component={UserAdd} />
                <Route path="/admin/users-data/:id" component={UserData} />
                <Route path="/admin/subscriptions" component={Subscriptions} />
            </Switch>
        );
    }
}
