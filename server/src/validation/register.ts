var validator = require("validator");
var _ = require("lodash");
import { httpsQuery } from "../models/db";

interface errors {
    prefix?: string;
    email?: string;
    phone?: string;
    country?: string;
    permissions?: string;
    other?: string;
}

let errors = <errors>{};

/**
 * vérifie si l'adresse email à enregistrer n'existe pas déjà et si tous les champs sont bien remplis. Renvoie un callback
 * @param data 
 * @param callback 
 */
export default function verifyRegister(data, callback) {
    errors = <errors>{};
    verifyFields(data);
    verifyUniqueEmail(data, errors => {
        callback(errors, _.isEmpty(errors));
    });
}


/**
 * Vérifie que l'aaddresse email à enregistrer n'existe pas déjà. Renvoie un callback
 * @param data 
 * @param callback 
 */
function verifyUniqueEmail(data, callback) {
    const query = `select CERHIS_MONITOR.* from CERHIS_MONITOR where CERHIS_MONITOR.email='${
            data.email
        }'`

    httpsQuery(query, (err, res, meta) => {
        if (!err) {
            res.forEach(user => {
                if (user.id != data.id) {
                    errors.email = "email déjà existant!";
                }
            });
        } else {
            errors.other = "Une erreur est apparue";
        }
        callback(errors);
    });
}

function verifyFields(data) {
    if (validator.isEmpty(data.prefix)) {
        errors.prefix = "Ce champs est requis";
    }

    if (validator.isEmpty(data.email)) {
        errors.email = "Ce champs est requis";
    }

    if (validator.isEmpty(data.phone)) {
        errors.phone = "Ce champ est requis";
    }

    if (validator.isEmpty(data.country)) {
        errors.country = "Ce champ est requis";
    }

    if (validator.isEmpty(data.permissions)) {
        errors.permissions = "Ce champ est requis";
    }
}
