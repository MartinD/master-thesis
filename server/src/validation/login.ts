var validator = require("validator")
var _ = require("lodash")

interface error{
    email ?: string,
    password ?: string
}

export default function validateInput(data){

    var errors = <error>{}

    if(validator.isEmpty(data.email)){
        errors.email = "Ce champs est requis"
    }

    if(validator.isEmpty(data.password)){
        errors.password = "Ce champs est requis"
    }

    return {
        errors,
        isValid: _.isEmpty(errors)
    }

}