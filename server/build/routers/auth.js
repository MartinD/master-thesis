"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("dotenv").config();
var express = require("express");
var _ = require("lodash");
var router = express.Router();
var jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const login_1 = require("../validation/login");
const db_1 = require("../models/db");
router.post("/", (req, res) => {
    const { errors, isValid } = login_1.default(req.body);
    if (!isValid) {
        res.status(400).json(errors);
    }
    const query = `SELECT CERHIS_MONITOR.* FROM CERHIS_MONITOR WHERE email='${req.body.email}'`;
    db_1.httpsQuery(query, (err, user) => {
        if (!_.isEmpty(user)) {
            if (bcrypt.compareSync(req.body.password, user[0].pswd)) {
                const token = jwt.sign({
                    id: user[0].id,
                    email: user[0].email,
                    name: user[0].name,
                    firstname: user[0].firstname,
                    phone: user[0].phone,
                    prefix: user[0].prefix,
                    country: user[0].country,
                    permissions: user[0].permissions,
                    type: user[0].type
                }, process.env.JWT_SECRET);
                res.json({ token });
            }
            else {
                res
                    .status(401)
                    .json({ errors: { form: "Identifiants invalides" } });
            }
        }
        else {
            res
                .status(401)
                .json({ errors: { form: "Identifiants invalides" } });
        }
    });
});
exports.default = router;
