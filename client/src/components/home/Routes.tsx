import * as React from "react"
import { Route, Switch } from "react-router-dom"
import { Home } from "./pages/Home"

export class Routes extends React.Component<any, any>{

    render(){
        return(
            <Switch>
                <Route to="/" component={Home} />
            </Switch>
        )
    }

}