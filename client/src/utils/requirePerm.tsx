import * as React from "react"
import { connect } from "react-redux"
import { Redirect } from "react-router-dom"

export default function Permissions(options ?: any){
    return (ComponentClass) => {
        return connect( state => {
            return {
                auth : state.auth
            }
        })(class CustomClass extends React.Component<any,any>{
            render(){
                if(this.props.auth.user.permissions == "root" || this.props.auth.user.permissions == "admin"){
                    return (
                        <ComponentClass {...this.props}></ComponentClass>
                    ) 
                }
				return <Redirect to="/"/>
            }
        })
    }
}
